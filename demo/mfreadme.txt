MinForth Demos
==============

This directory comprises some popular public little Forth programs,
some special MinForth code snippets, and some packages copied from
their web site
https://theforth.net/

Adaptations to MinForth, if any, are marked in the code and/or
documentation. 

Please observe the copyrights of the program/package authors.
