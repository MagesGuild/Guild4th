------------------------------------------------------------------------------
MinForth V3.4 - Building turnkey applications
------------------------------------------------------------------------------

Turnkey applications are ready-to-use executable programs without disclosure
of their source code. They are often provided in combination with some
protection against unprivileged access and tampering.

MinForth offers two different methods for building turnkey applications.

A) When the application source can be processed by mf2c:
   1. transpile and compile tha application
   2. the user receives the compiled executable file
   
   Example in demo/reduce directory 
 
B) When the application source cannot be processed by mf2c:
   1. use or build an auto-starting and auto-loading MinForth variant
   2. save an overlay of the loaded turnkey application
   3. the user receives the overlay file with the auto-loading MinForth variant


1. Demo Application Example (mfchkfile.mf)
------------------------------------------

Start MinForth and include file mfchkfile.mf:

> mf3
┌────────────────────────┐
│ MinForth V3.4 - 32 bit │
└────────────────────────┘
3999116 bytes free
# include mfchkfile.mf
Enter file name: TreasureIsland.txt 
File checksum is: BCEDF3E8F8401771  ok
#

The application works, matching the checksum in file TreasureIsland.chk
It can be compiled in 32-bit or 64-bit version alike.

But as it is, it allows access to the full Forth interpreter, which is not
acceptable for turnkey applications. And it has to be converted to become a
stand-alone program that starts automatically.


2. Making a Stand-Alone Application (mfchksum.mf)
----------------------------------------------

The above application mfchkfile.mf generates only checksums. We want a better
application called mfchksum, that also checks a given checksum against a given
file.

When the given checksum is identical to the calculated file checksum, a brief
message and GOOD shall be displayed, otherwise FAILURE shall be displayed and
the application's OS return code shall be non-zero.

It shall be compilable as 32-bit or 64-bit version.

Usage:
mfchksum <infile> > <outfile>  write infile checksum and file name to outfile
mfchksum -c <outfile>          check checksum of infile read from outfile
mfchksum -q <outfile>          for quiet file chechsum check in scripts

Only for development and testing we build a version that takes MinForth's
command line arguments as program arguments. We will change this later.
Checksum and reference text file name is prvided in file TrIsl.chk

Tests:

> mf3 -c TrIsl.chk
┌──────────────────────────┐
│ MinForth V3.4.5 - 32 bit │
└──────────────────────────┘
999076 bytes free
# include mfchksum.mf  BCEDF3E8F8401771  TreasureIsland.txt  GOOD
# bye

Good job. (change one letter in the reference text and try again) 

> mf3  \ without file parameter
┌──────────────────────────┐
│ MinForth V3.4.5 - 32 bit │
└──────────────────────────┘
999076 bytes free
# include mfchksum.mf  BCEDF3E8F8401771
Usage:
mfcksum [-c|-q] <file>
 Options:
  -c read checksum from file and check with file
  -q quiet mode (return parameter -1 on failure)
# bye

Good. We got the usage hint now.

The application works, but command-line parameters have to be entered while
starting a Forth session. This is awkward and the system is unprotected.


3. Constructing a Turnkey Application (filecheck)
-------------------------------------------------

For a turnkey application, we want the new program to start with automatic
loading of an overlay file. The overlay comprises our above checksum program
in illegible binary form. 

For an auto-loading system we need a modified version of MinForth's mf3.mfc.

You should now study the little enclosed file filecheck.mfc now. It defines
a compact MinForth system with an new word _FILECHECKBOOT in its boot
squence. The new word
- either loads and executes an overlay file filecheck.ovl
- or asks for a passkey to grant access to MinForth standard interpreter.

Copy filecheck.mfc to the src directory, enter the commands
> mf2c filecheck
> tc32 filecheck
and copy the new compiled filecheck.exe back to the first directory
e.g. .\MF34\demo\turnkey

Now we need an overlay file.

Copy the last file mfchksum.mf to a new file filecheck.mf. Therein delete the
last line with the single command MFCHKSUM. Then execute

> filecheck
Passkey? 1234  \ a real good one ;-)
(we are now through the backdoor in a normal MinForth session)
# include filecheck.mf   ok
# save-overlay filecheck.ovl   ok
# bye

Final Tests:

> filecheck
Usage:
mfcksum [-c|-q] <file>
 Options:
  -c read checksum from file and check with file
  -q quiet mode (return parameter -1 on failure)

d:\MF34\demo\turnkey>filecheck test.txt
3AD5964A0A088DDF  test.txt

d:\MF34\demo\turnkey>filecheck -c test.chk
3AD5964A0A088DDF  test.txt  GOOD

Now we have our turnkey application. It consists of 2 files:
filecheck.exe  and  filecheck.ovl.

Further improvements are left as exercise:
- implement a better passkey
- eliminate debugging tools from tools.mfc (an attacker would find them nice)
