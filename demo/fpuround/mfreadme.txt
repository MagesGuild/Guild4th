------------------------------------------------------------------------------
MinForth V3.4 - Change FPU rounding modes
------------------------------------------------------------------------------

Usage:

Include fpuround.mfc into wordset list within mf3.mfc
and rebuild minforth with eg
./mf2c
./lmake mf3

Available new commands:

FE-GETROUND  \ ( -- m ) get actual FPU rounding mode
FE-SETROUND  \ ( m -- ) set FPU rounding mode
FE-TONEAREST  \ ( -- ) set default FPU rounding mode
FE-TOWARDZERO  \ ( -- ) set FPU rounding mode toward zeros
FE-UPWARD  \ ( -- ) set FPU rounding mode toward positive infinity
FE-DOWNWARD  \ ( -- ) set FPU rounding mode toward negative infinity

Caution:

By default MinForth does not change the rounding mode but 'inherits' it from
the calling process, which is in about 100% the round-to-nearest mode.

User application specific rounding schemes should reset the rounding mode
on return to avoid possible 'strange' results from called library functions
afterwards.
