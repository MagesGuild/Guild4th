------------------------------------------------------------------------------
MinForth V3.4 - Overlays
------------------------------------------------------------------------------

Overlays are binary copies of a MinForth dataspace mapped to a disk file.
Primitives in MinForth codespace are not affected.

Support for overlays is available in MinForth when the file overlay.mfc had
been included in the build (e.g. as otional wordset within mf3.mfc) 

When working with overlays, the MinForth systems between saving and loading
overlay files must be identical, ie. overlays from different builds are not
accepted.

The loaded overlay overwrites the current dataspace and current high-level
definitions therein and replaces them with the dataspace content at the time
of saving the overlay. Forth system variables like STATE or BASE are not
affected when reloading an overlay. 

Usage:

If not already included, copy overlay.mfc to src directory
Rebuild MinForth with overlay.mfc included eg. appended to wordsets in mf3.mfc

Example:

start new MinForth session:
> mf3

define a new word
# variable testvar  7 testvar !

check dirctionary with TESTVAR as first entry
# words

save an overlay
# save-overlay ovl.ovl

exit MinForth
# bye

---

start new MinForth session:
> mf3

check dirctionary does not have TESTVAR
# words

load the overlay
# save-overlay ovl.ovl

check if TESTVAR is there
# testvar ?  7  ok

exit MinForth
#bye

