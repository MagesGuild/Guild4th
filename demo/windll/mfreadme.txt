------------------------------------------------------------------------------
MinForth V3.4 - Dynamic Linking of Windows DLL Functions
------------------------------------------------------------------------------

A) Example for compiled single function linking
-----------------------------------------------

1. Plain C test code in mbox.c to show a simple Windows Message Box called
   through dynamic linkage with Windows user32.dll library

2. Ported as nearly unmodified C code to new MinForth word WIN_MB_YN

3. Copy mbox.mfc to src directory and append new line
    #INCLUDE mbox.mfc
   to wordset list in mf3.mfc and rebuild MinForth
   
4. In new Minforth enter
   S" Just asking" S" Got it?" WIN_MB_YN 

   
B) Example for interpreted dynamic linking through MinForth words
-----------------------------------------------------------------

1. Defined new words (understand the sources in windll.mfc)
    NULL       often used in Windows programming
    Z"         zero terminated strings as in C
    WINLOADLL  get DLL handle
    WINGETFUNC get function address
    WINFUNC/0  execute function and fetch 0..5 parameters from MF stack
    ...
    WINFUNC/5
	
2. Copy windll.mfc to src directory and append new line
    #INCLUDE windll.mfc
   (erase old line #INCLUDE mbox.mfc)
   to wordset list in mf3.mfc and rebuild MinForth
   
3. Start new MinForth and enter
    NULL Z" Got it?" Z" Just asking" 4 ( MB_YESNO constant from winuser.h )
    S" user32.dll" WINLOADDLL
    S" MessageBoxA" WINGETFUNC 
    WINFUNC/4
