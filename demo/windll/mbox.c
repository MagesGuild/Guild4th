/* ======================================================
   Windows Message Box Example
   ======================================================
*/

#include <stdio.h>
#include <windows.h>

int main (void) {

  HMODULE dllh = LoadLibrary("user32.dll");
  if (!dllh) return -1001; // error bad dll
  
  FARPROC mbox = GetProcAddress(dllh,"MessageBoxA");
  if (!mbox) return -1002; // error bad function
// printf("[%p]",mbox);  // check address with dll-export-viewer

  int yn = mbox(0,"Got it?","Just asking",MB_YESNO);
  
  printf("\nGot button code [%d]\n",yn);
}

