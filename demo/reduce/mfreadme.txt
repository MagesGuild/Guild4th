------------------------------------------------------------------------------
MinForth V3.4 - Codesize Reduction
------------------------------------------------------------------------------

Copy the files in this directory to the src directory.


A) Application Testing in high-level Forth
------------------------------------------

1. Start MinForth and include Pascal's triangle example:
   mf3 /i triangle.mf

┌────────────────────────┐
│ MinForth V3.4 - 32 bit │
└────────────────────────┘
199116 bytes free
                           1
                         1   1
                       1   2   1
                     1   3   3   1
                   1   4   6   4   1
                 1   5  10  10   5   1
               1   6  15  20  15   6   1
             1   7  21  35  35  21   7   1
           1   8  28  56  70  56  28   8   1
         1   9  36  84 126 126  84  36   9   1
       1  10  45 120 210 252 210 120  45  10   1
     1  11  55 165 330 462 462 330 165  55  11   1
   1  12  66 220 495 792 924 792 495 220  66  12   1

So we have checked that the triangle program works fine.


B) Trying to convert it for transpilation to C with MF2C 
--------------------------------------------------------

Tentative simple linear copy to an mfc file --> see file triangle1.mfc

Try to transpile it:
copy triangle1.mfc to src directory
enter: mf2c triangle1.mfc

MinForth V3.4 to C Transpiler
-----------------------------
Reading Forth source file triangle1.mfc
Including core.mfc
Error in file triangle1.mfc line 13:
        ?do dup 4 .r j i - * i 1+ / loop
--------^ ?? nested ?DO..LOOPs not allowed
(press return)

The error message says it all. We have to factor the inner loop out.
This had been done in source file triangle2.mfc
enter: mf2c triangle2  (the suffix .mfc is added automatically)

MinForth V3.4 to C Transpiler
-----------------------------
Reading Forth source file triangle2.mfc
Including core.mfc
Total definitions: 364
 - in dictionary:  326
 - therein hidden: 96
Writing C target file triangle2.c

compile it:
tc32 triangle2  (the suffix .c is added automatically)
Making triangle2.exe with tcc 32 bit

run it:
triangle2

                           1
                         1   1
                       1   2   1
                     1   3   3   1
                   1   4   6   4   1
                 1   5  10  10   5   1
               1   6  15  20  15   6   1
             1   7  21  35  35  21   7   1
           1   8  28  56  70  56  28   8   1
         1   9  36  84 126 126  84  36   9   1
       1  10  45 120 210 252 210 120  45  10   1
     1  11  55 165 330 462 462 330 165  55  11   1
   1  12  66 220 495 792 924 792 495 220  66  12   1
  
It works as expected.


C) Reducing Code Size
---------------------

Inspection of the generated file triangle2.c reveals that is is about 97k
big. This is very múch, compared with the original triangle2.mfc file size of
just 426 bytes.

Explanation: we included core.mfc, which comprises a full Forth system that is
not required by the triangle2 program.

Solution: add the transpiler command #REDUCE to the mfc source.

This had been done in source file triangle3.mfc
enter: mf2c triangle3 

MinForth V3.4 to C Transpiler
-----------------------------
Reading Forth source file triangle3.mfc
Including core.mfc
Total definitions: 364
 - in dictionary:  47
 - therein hidden: 4
Writing C target file triangle3.c

The number of words in the dictionary shrank from 326 to 47. The file size of
triangle3.c shrank to only about 11k.

compile and runit:
tc32 triangle3
Making triangle3.exe with tcc 32 bit
triangle3

                           1
                         1   1
                       1   2   1
                     1   3   3   1
                   1   4   6   4   1
                 1   5  10  10   5   1
               1   6  15  20  15   6   1
             1   7  21  35  35  21   7   1
           1   8  28  56  70  56  28   8   1
         1   9  36  84 126 126  84  36   9   1
       1  10  45 120 210 252 210 120  45  10   1
     1  11  55 165 330 462 462 330 165  55  11   1
   1  12  66 220 495 792 924 792 495 220  66  12   1
  
It works as expected, but now much smaller.  :-)
