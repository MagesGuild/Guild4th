/* ----------- MinForth C Target -----------
   - transpiled by emf2c ----- do not edit -
   - Forth source : triangle3.mfc
   - built Mon Jan 25 17:53:07 2021

   (see license conditions in mflicense.txt)
*/

#include "mf3.h"

// --- Forward Declarations ---

static inline void mf67FCEB09(void); // >R
static inline void mf130456D1(void); // R>
static inline void mf7D04FDAF(void); // R@
static inline void mf0807143C(void); // RDROP
static inline void mf52C16B0C(void); // DROP
static inline void mf87E8362E(void); // ROT
static inline void mf57E80646(void); // DUP
static inline void mf2FE7860F(void); // OVER
static inline void mfEF6476DC(void); // 2DROP
static inline void mfE0839FF6(void); // 2DUP
static inline void mf7CE4AA04(void); // OR
static inline void mf15ED5F69(void); // 0<
static inline void mf2E0C9DAA(void); // +
static inline void mf280C9438(void); // -
static inline void mf26EB3B95(void); // 1+
static inline void mf20EB3223(void); // 1-
static inline void mf6C84489B(void); // ABS
static inline void mf2F0C9F3D(void); // *
static inline void mf2A0C975E(void); // /
static inline void mfA7F2C26D(void); // 2*
void mf1691D15F(void); // _MU/MOD
static inline void mf97DA4729(void); // C!
static inline void mf838F7252(void); // CELLS
static inline void mfC50BF85F(void); // @
static inline void mf240C8DEC(void); // !
static inline void mfD148A1CA(void); // _DP
static inline void mf5F2B6B8B(void); // HERE
void mf9E0B1B0A(void); // PAD
void mfE972DB58(void); // BASE
static inline void mfD9BC5E8A(void); // EMIT
void mf20DD5D6B(void); // BL
void mf1808F0E5(void); // SPACE
void mf8C12EE82(void); // SPACES
static inline void mf64D9F6E0(void); // CR
void mf142FE78D(void); // TYPE
void mf6B388877(void); // _N>DIGIT
void mf4DCEA979(void); // __HLD
void mfEBB64F78(void); // HOLD
void mfA6F73E08(void); // <#
void mf260C9112(void); // #
void mf11C88844(void); // #>
void mf26C8A953(void); // #S
void mf0AADBFA4(void); // SIGN
void mfE4E51507(void); // _(.)
void mf67D44899(void); // .R
void mfEA5BAF16(void); // INNER_LOOP
void mf988F50CC(void); // PASCTRIANGLE
void mf96272888(void); // MAIN

// --- Forth Dictionary ---

mfHdr mfdict[48]={{NULL,0,NULL,NULL},
{mfdict,0,"\002>R",mf67FCEB09},
{mfdict+1,0,"\002R>",mf130456D1},
{mfdict+2,0,"\002R@",mf7D04FDAF},
{mfdict+3,0,"\005RDROP",mf0807143C},
{mfdict+4,0,"\004DROP",mf52C16B0C},
{mfdict+5,0,"\003ROT",mf87E8362E},
{mfdict+6,0,"\003DUP",mf57E80646},
{mfdict+7,0,"\004OVER",mf2FE7860F},
{mfdict+8,0,"\0052DROP",mfEF6476DC},
{mfdict+9,0,"\0042DUP",mfE0839FF6},
{mfdict+10,0,"\002OR",mf7CE4AA04},
{mfdict+11,0,"\0020<",mf15ED5F69},
{mfdict+12,0,"\001+",mf2E0C9DAA},
{mfdict+13,0,"\001-",mf280C9438},
{mfdict+14,0,"\0021+",mf26EB3B95},
{mfdict+15,0,"\0021-",mf20EB3223},
{mfdict+16,0,"\003ABS",mf6C84489B},
{mfdict+17,0,"\001*",mf2F0C9F3D},
{mfdict+18,0,"\001/",mf2A0C975E},
{mfdict+19,0,"\0022*",mfA7F2C26D},
{mfdict,0,"\007_MU/MOD",mf1691D15F},
{mfdict+20,0,"\002C!",mf97DA4729},
{mfdict+22,0,"\005CELLS",mf838F7252},
{mfdict+23,0,"\001@",mfC50BF85F},
{mfdict+24,0,"\001!",mf240C8DEC},
{mfdict,0,"\003_DP",mfD148A1CA},
{mfdict+25,0,"\004HERE",mf5F2B6B8B},
{mfdict+27,0,"\003PAD",mf9E0B1B0A},
{mfdict+28,0,"\004BASE",mfE972DB58},
{mfdict+29,0,"\004EMIT",mfD9BC5E8A},
{mfdict+30,0,"\002BL",mf20DD5D6B},
{mfdict+31,0,"\005SPACE",mf1808F0E5},
{mfdict+32,0,"\006SPACES",mf8C12EE82},
{mfdict+33,0,"\002CR",mf64D9F6E0},
{mfdict+34,0,"\004TYPE",mf142FE78D},
{mfdict,0,"\010_N>DIGIT",mf6B388877},
{mfdict+35,0,"\004HOLD",mfEBB64F78},
{mfdict+37,0,"\002<#",mfA6F73E08},
{mfdict+38,0,"\001#",mf260C9112},
{mfdict+39,0,"\002#>",mf11C88844},
{mfdict+40,0,"\002#S",mf26C8A953},
{mfdict+41,0,"\004SIGN",mf0AADBFA4},
{mfdict,0,"\004_(.)",mfE4E51507},
{mfdict+42,0,"\002.R",mf67D44899},
{mfdict+44,0,"\012INNER_LOOP",mfEA5BAF16},
{mfdict+45,0,"\014PASCTRIANGLE",mf988F50CC},
{mfdict+46,0,"\004MAIN",mf96272888}};

// --- Search Parameters

#define MFPRIMS 47
void* MFLAST=mfdict+MFPRIMS;
void* MFLATEST=mfdict+MFPRIMS;
void* MFFWL[3]={mfdict+MFPRIMS,"\005FORTH",NULL};
void* MFCTX[8]={MFFWL,NULL};

#include "mf3.sys"

// --- Forth Definitions

#define MFCORE 

static inline void mf67FCEB09(void) { // : >R
  mferr(mfin(1);mfrout(1)); 
  mfrpush(mfpop());
}

static inline void mf130456D1(void) { // : R>
  mferr(mfrin(1);mfout(1)); 
  mfpush(mfrpop());
}

static inline void mf7D04FDAF(void) { // : R@
  mferr(mfrin(1);mfout(1)); 
  mfpush(mfrtos);
}

static inline void mf0807143C(void) { // : RDROP
  mferr(mfrin(1)); 
  mfrdrop;
}

static inline void mf52C16B0C(void) { // : DROP
  mferr(mfin(1)); 
  mfdrop;
}

static inline void mf87E8362E(void) { // : ROT
  mferr(mfin(3)); 
  mfCell a=mfthd; 
  mfthd=mfsec,mfsec=mftos,mftos=a;
}

static inline void mf57E80646(void) { // : DUP
  mferr(mfin(1);mfout(1)); 
  mfup, mftos=mfsec;
}

static inline void mf2FE7860F(void) { // : OVER
  mferr(mfin(2);mfout(1)); 
  mfup, mftos=mfthd;
}

static inline void mfEF6476DC(void) { // : 2DROP
  mferr(mfin(2)); 
  mf2drop;
}

static inline void mfE0839FF6(void) { // : 2DUP
  mferr(mfin(2);mfout(2)); 
  mfCell a=mfsec, b=mftos; 
  mfpush(a), mfpush(b);
}

static inline void mf7CE4AA04(void) { // : OR
  mferr(mfin(2)); 
  mfdrop, mftos|=mfontos;
}

static inline void mf15ED5F69(void) { // : 0<
  mferr(mfin(1)); 
  mftos=-(mftos<0);
}

static inline void mf2E0C9DAA(void) { // : +
  mferr(mfin(2)); 
  mfdrop, mftos+=mfontos;
}

static inline void mf280C9438(void) { // : -
  mferr(mfin(2)); 
  mfdrop, mftos-=mfontos;
}

static inline void mf26EB3B95(void) { // : 1+
  mferr(mfin(1)); 
  mftos+=1;
}

static inline void mf20EB3223(void) { // : 1-
  mferr(mfin(1)); 
  mftos-=1;
}

static inline void mf6C84489B(void) { // : ABS
  mferr(mfin(1)); 
  mftos=llabs(mftos);
}

static inline void mf2F0C9F3D(void) { // : *
  mferr(mfin(2)); 
  mfdrop, mftos*=mfontos;
}

static inline void mf2A0C975E(void) { // : /
  mferr(mfin(2);mfzero(mftos)); 
  mfdrop, mftos/=mfontos;
}

static inline void mfA7F2C26D(void) { // : 2*
  mferr(mfin(1)); 
  mftos<<=1;
}

void mf1691D15F(void) { // : _MU/MOD
  mferr(mfin(3);mfzero(mftos)); 
#ifdef MFDBL 
  mfUDbl udq, ud=*(mfUDbl*)(mfsp-2); mfUCell v=mftos; 
  if(ud==v) mfthd=0, mfsec=1, mftos=0; 
  else mfdtos=udq=ud/v, mfthd=ud-udq*v; 
#else 
  mfUCell v=mftos, a, b=mfthd, c=mfsec, d; 
  if (!c&&(b==v)) a=1, b=c=0; else { 
    if (!c) a=b/v, c=b-a*v, b=0; else { 
      a=c/v, c-=a*v;   
      for (int i=0; i<64; i++) { 
        d=c>>63, c=(c<<1)|(b>>63), b=(b<<1)|(a>>63), a<<=1; 
        if ((c>=v)||d) c-=v, a+=1; } } } 
  mfsec=a, mftos=b, mfthd=c; 
#endif 
}

static inline void mf97DA4729(void) { // : C!
  mferr(mfin(2)); 
  mfcat(mftos)=(mfChar)mfsec, mf2drop;
}

static inline void mf838F7252(void) { // : CELLS
  mferr(mfin(1)); 
  mftos*=MFSIZE;
}

static inline void mfC50BF85F(void) { // : @
  mferr(mfin(1)); 
  mftos=mfat(mftos);
}

static inline void mf240C8DEC(void) { // : !
  mferr(mfin(2)); 
  mfat(mftos)=mfsec, mf2drop;
}

static inline void mfD148A1CA(void) { // : _DP
  mferr(mfout(1)); 
  mfpush(&mfdp);
}

static inline void mf5F2B6B8B(void) { // : HERE
  mfD148A1CA(); // _DP
  mfC50BF85F(); // @
}

void mf9E0B1B0A(void) { // : PAD
  mf5F2B6B8B(); // HERE
  mfpush(264); // $108
  mf2E0C9DAA(); // +
  mfpush(22); // $16
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
}

mfCell MFE972DB58=10; // 10

void mfE972DB58(void) { // VARIABLE BASE
  mfpush(&MFE972DB58);
}

static inline void mfD9BC5E8A(void) { // : EMIT
  mferr(mfin(1)); 
  mfemit(mfpop());
}

const mfCell MF20DD5D6B=32*MFSIZE; // 32

void mf20DD5D6B(void) { // CONSTANT BL
  mfpush(MF20DD5D6B);
}

void mf1808F0E5(void) { // : SPACE
  mfpush(MF20DD5D6B); // BL
  mfD9BC5E8A(); // EMIT
}

void mf8C12EE82(void) { // : SPACES
mfFOR
  mf1808F0E5(); // SPACE
mfNEXT
}

static inline void mf64D9F6E0(void) { // : CR
  mfemit('\n');
}

void mf142FE78D(void) { // : TYPE
  mferr(mfin(2)); 
  mftype(mfsec,mftos), mf2drop;
}

void mf6B388877(void) { // : _N>DIGIT
  mfpush(&MFE972DB58); // BASE
  mfC50BF85F(); // @
  mfsec=mfn2dig(mfsec,mftos); mftos=(mfsec==256 ? 0 : -1);
}

mfCell MF4DCEA979=0; // 0

void mf4DCEA979(void) { // VARIABLE __HLD
  mfpush(&MF4DCEA979);
}

void mfEBB64F78(void) { // : HOLD
  mfpush(&MF4DCEA979); // __HLD
  mfC50BF85F(); // @
  mf20EB3223(); // 1-
  mf57E80646(); // DUP
  mfpush(&MF4DCEA979); // __HLD
  mf240C8DEC(); // !
  mf97DA4729(); // C!
}

void mfA6F73E08(void) { // : <#
  mf9E0B1B0A(); // PAD
  mf20EB3223(); // 1-
  mfpush(0); // $0
  mf2FE7860F(); // OVER
  mf97DA4729(); // C!
  mfpush(&MF4DCEA979); // __HLD
  mf240C8DEC(); // !
}

void mf260C9112(void) { // : #
  mfpush(&MFE972DB58); // BASE
  mfC50BF85F(); // @
  mf1691D15F(); // _MU/MOD
  mf87E8362E(); // ROT
  mf6B388877(); // _N>DIGIT
  mf52C16B0C(); // DROP
  mfEBB64F78(); // HOLD
}

void mf11C88844(void) { // : #>
  mfEF6476DC(); // 2DROP
  mfpush(&MF4DCEA979); // __HLD
  mfC50BF85F(); // @
  mf9E0B1B0A(); // PAD
  mf20EB3223(); // 1-
  mf2FE7860F(); // OVER
  mf280C9438(); // -
}

void mf26C8A953(void) { // : #S
mfBEGIN
  mf260C9112(); // #
  mfE0839FF6(); // 2DUP
  mf7CE4AA04(); // OR
mfNOTUNTIL
}

void mf0AADBFA4(void) { // : SIGN
  mf15ED5F69(); // 0<
mfIF
  mfpush(45); // $2D
  mfEBB64F78(); // HOLD
mfTHEN
}

void mfE4E51507(void) { // : _(.)
  mf57E80646(); // DUP
  mf6C84489B(); // ABS
  mfpush(0); // $0
  mfA6F73E08(); // <#
  mf26C8A953(); // #S
  mf87E8362E(); // ROT
  mf0AADBFA4(); // SIGN
  mf11C88844(); // #>
}

void mf67D44899(void) { // : .R
  mf67FCEB09(); // >R
  mfE4E51507(); // _(.)
  mf130456D1(); // R>
  mf2FE7860F(); // OVER
  mf280C9438(); // -
  mf8C12EE82(); // SPACES
  mf142FE78D(); // TYPE
}

void mfEA5BAF16(void) { // : INNER_LOOP
  mf2FE7860F(); // OVER
  mf20EB3223(); // 1-
  mf67FCEB09(); // >R
mfQDO
  mf57E80646(); // DUP
  mfpush(4); // $4
  mf67D44899(); // .R
  mf7D04FDAF(); // R@
mfI
  mf280C9438(); // -
  mf2F0C9F3D(); // *
mfI
  mf26EB3B95(); // 1+
  mf2A0C975E(); // /
mfLOOP
  mf0807143C(); // RDROP
}

void mf988F50CC(void) { // : PASCTRIANGLE
  mf64D9F6E0(); // CR
  mf57E80646(); // DUP
  mfpush(0); // $0
mfQDO
  mfpush(1); // $1
  mf2FE7860F(); // OVER
  mf20EB3223(); // 1-
mfI
  mf280C9438(); // -
  mfA7F2C26D(); // 2*
  mf8C12EE82(); // SPACES
mfI
  mf26EB3B95(); // 1+
  mfpush(0); // $0
  mfEA5BAF16(); // INNER_LOOP
  mf64D9F6E0(); // CR
  mf52C16B0C(); // DROP
mfLOOP
  mf52C16B0C(); // DROP
}

void mf96272888(void) { // : MAIN
  mfpush(13); // $D
  mf988F50CC(); // PASCTRIANGLE
}

// --- C Main ---

int main(int argc, char* argv[]){
  mfstk=malloc(mfstksize*MFSIZE),mfsp=mfstk,mflp=mfstk+mfstksize;
  mfrst=malloc(mfrstsize*MFSIZE),mfrp=mfrst;
  mffst=malloc(mffstsize*MFSIZE*MFFLTSIZE),mffp=mffst;
  mfargc=argc, mfargv=argv;
  setjmp(mfabort);
  mf96272888(); // MAIN
}

// --- End ---
