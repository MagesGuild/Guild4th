Forth Scientific Library
------------------------

with microscopic adaptations to MinForth e.g. added file inclusions,
and renamed file names to match the overview in status.txt.

Example:
  mf3
  include 18_gamma.seq
  gamma-test

Notes:

Many test codes are only compiled when the flag value TEST-CODE? is set.
The flag should already be set within file fsl.util.fs.

It is recommended to set WARNING OFF for better visibility.

You have to study the sources.
