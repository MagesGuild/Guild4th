/* ----------- MinForth C Target -----------
   - transpiled by emf2c ----- do not edit -
   - Forth source : emf3.mfc
   - built Tue Jan 26 11:26:54 2021

   (see license conditions in mflicense.txt)
*/

#include "emf3.h"
#include "emf3.sys"

// --- Forward Declarations ---

void mfE5B4B40F(void); // THROW
void mf8B51E6F7(void); // _RP!
static inline void mf67FCEB09(void); // >R
static inline void mf130456D1(void); // R>
static inline void mf7D04FDAF(void); // R@
void mf89C518BC(void); // _SP!
void mfCE61558A(void); // DEPTH
static inline void mf52C16B0C(void); // DROP
static inline void mfA2DF200E(void); // SWAP
static inline void mf87E8362E(void); // ROT
static inline void mf57E80646(void); // DUP
static inline void mf2FE7860F(void); // OVER
static inline void mf8003A4B3(void); // ?DUP
static inline void mfE67669F8(void); // PICK
static inline void mf63DECBC8(void); // NIP
static inline void mf93A82DE8(void); // TUCK
void mf5451721E(void); // ROLL
static inline void mfEF6476DC(void); // 2DROP
static inline void mfE0839FF6(void); // 2DUP
static inline void mf8C8BC01F(void); // 2OVER
static inline void mf404D123E(void); // 2SWAP
static inline void mf89F60699(void); // 2>R
static inline void mf34FF0561(void); // 2R>
static inline void mf3EFF151F(void); // 2R@
void mfAC2CA76D(void); // NOOP
void mf501B0925(void); // TRUE
void mfEE597878(void); // FALSE
static inline void mf6B09DC74(void); // _MSB
static inline void mf91666DC6(void); // AND
static inline void mf7CE4AA04(void); // OR
static inline void mf4F46575E(void); // XOR
static inline void mfD745D921(void); // INVERT
static inline void mf84439159(void); // LSHIFT
static inline void mf24728B77(void); // RSHIFT
static inline void mf14ED5DD6(void); // 0=
static inline void mf15ED5F69(void); // 0<
static inline void mf13ED5C43(void); // 0>
static inline void mfDBAD15F5(void); // 0<>
static inline void mf380CAD68(void); // =
static inline void mf390CAEFB(void); // <
static inline void mf0EF30764(void); // U<
static inline void mf3B0CB221(void); // >
static inline void mf93F7201F(void); // <>
static inline void mf10F30A8A(void); // U>
static inline void mf2E0C9DAA(void); // +
static inline void mf280C9438(void); // -
static inline void mf5EE442DB(void); // NEGATE
static inline void mf26EB3B95(void); // 1+
static inline void mf20EB3223(void); // 1-
static inline void mf6C84489B(void); // ABS
static inline void mf2F0C9F3D(void); // *
void mf45BE23A2(void); // /MOD
static inline void mf2A0C975E(void); // /
static inline void mf6453F3A3(void); // MOD
static inline void mfA7F2C26D(void); // 2*
static inline void mfA2F2BA8E(void); // 2/
void mf09D242EA(void); // WITHIN
void mf1C599279(void); // MAX
void mf0E45F4B7(void); // MIN
void mfE40F181D(void); // _MU+
void mfE30F168A(void); // _MU*
void mf1691D15F(void); // _MU/MOD
void mfD9ACE697(void); // UM*
void mf5F2279EC(void); // UM/MOD
void mf16478CF0(void); // S>D
void mf8B105859(void); // DNEGATE
void mfE23549BD(void); // DABS
void mf1CDF95B6(void); // M*
void mf1705E412(void); // SM/REM
void mf66F1FDC7(void); // FM/MOD
void mfE5A71F0A(void); // */MOD
void mf22DE6956(void); // */
static inline void mf2FEE4BC2(void); // CHAR+
void mfC7EDA80A(void); // CHARS
static inline void mf76DA1336(void); // C@
static inline void mf97DA4729(void); // C!
void mfC2064154(void); // MOVE
void mfAF3DC588(void); // FILL
void mfB7F60EB5(void); // ERASE
static inline void mfBB8FCA7A(void); // CELL+
static inline void mf838F7252(void); // CELLS
static inline void mfC50BF85F(void); // @
static inline void mf240C8DEC(void); // !
static inline void mf08DC01D1(void); // +!
static inline void mf3DF21B8F(void); // 2@
static inline void mf9CF2B11C(void); // 2!
void mfA3F7B2D4(void); // COUNT
void mf62A404AF(void); // /STRING
void mf2BEE4576(void); // CHAR/
void mf140219CD(void); // S=
void mfEE2653C2(void); // _UPPER
void mf10E38971(void); // _PLACE
void mf12E2D898(void); // _ORIGIN
void mf9F4C117D(void); // _LIMIT
static inline void mfD148A1CA(void); // _DP
static inline void mf5F2B6B8B(void); // HERE
void mf3FA1943F(void); // ALLOT
void mf901B2AFF(void); // ALIGNED
void mfAFA183BE(void); // ALIGN
void mf9E0B1B0A(void); // PAD
void mfCF430E1B(void); // UNUSED
void mfE972DB58(void); // BASE
static inline void mf48AF9A2C(void); // DECIMAL
static inline void mf818F192A(void); // HEX
void mf7A717643(void); // _DIGIT>N
void mf5773A68C(void); // >NUMBER
void mfDF58554B(void); // __DBL
void mfE37AEDD9(void); // __DPL
void mf60DFCE8E(void); // __BASE
void mfCC4D88AE(void); // __SIGN
void mf15208282(void); // __NUMADJ
void mf867FFF4D(void); // NUMBER?
static inline void mfD9BC5E8A(void); // EMIT
void mf20DD5D6B(void); // BL
void mf1808F0E5(void); // SPACE
void mf8C12EE82(void); // SPACES
static inline void mf64D9F6E0(void); // CR
void mf142FE78D(void); // TYPE
void mf6B388877(void); // _N>DIGIT
void mf4DCEA979(void); // __HLD
void mfEBB64F78(void); // HOLD
void mf3AFEA0B1(void); // HOLDS
void mfA6F73E08(void); // <#
void mf260C9112(void); // #
void mf11C88844(void); // #>
void mf26C8A953(void); // #S
void mf0AADBFA4(void); // SIGN
void mfE4E51507(void); // _(.)
void mf2B0C98F1(void); // .
void mf67D44899(void); // .R
void mfFD90D4E8(void); // _(U.)
void mf20F323BA(void); // U.
void mfC6C18638(void); // U.R
void mfACCAE4CC(void); // KEY
void mf209E17E9(void); // ACCEPT
void mf0119CBC8(void); // _#IB
void mfC49B66C5(void); // _TIB
void mfB3D82B11(void); // _SIBS
void mf0FF74456(void); // __SIBID
void mf12AE9718(void); // _SIB
void mfCFEC5BB6(void); // _>SIB
void mf01A69217(void); // _SRCSPEC
void mf7C1EEF6B(void); // _SRCLEN
void mf7A91D589(void); // _SRCADR
void mfFD898658(void); // _SRCID!
void mf5AF98C79(void); // _SRC2!
void mf600421B4(void); // _SRC>R
void mfD24E2A14(void); // _R>SRC
void mfB9790298(void); // SOURCE
void mf580B1E42(void); // >IN
void mf254113A2(void); // SOURCE-ID
void mfEF18D029(void); // _BLK
void mf94A8FFED(void); // SAVE-INPUT
void mfA10A7AB8(void); // RESTORE-INPUT
void mfC1D4E711(void); // REFILL
void mfD66336C5(void); // _SKIP
void mfC119E57B(void); // _SCAN
void mfB8B7D9B3(void); // __PRSAREA
void mf60C7C223(void); // __PRSOVF
void mfADFC5551(void); // _PARSE
void mfD918C10C(void); // PARSE
void mfF08832C0(void); // _PARSE-CHAR
void mfE9BCE12F(void); // _PARSE-NAME
void mfF92179F8(void); // PARSE-NAME
void mfF939F00F(void); // _PARSED
void mfA274DA18(void); // CAPS
void mf6D01E13D(void); // WORD
void mfC94E167C(void); // _PARSE-WORD
void mfA59F665D(void); // CHAR
void mfD90C17DB(void); // |
void mf2D0C9C17(void); // (
void mf9DD49D9B(void); // .(
void mf0E9A8AA9(void); // _FIND
void mf85A4C44F(void); // _WORDLISTS
void mf82CE5579(void); // _CONTEXT
void mf5AADB2E4(void); // _FIND-WORD
void mfFB42BA5A(void); // FIND
void mf220C8AC6(void); // '
void mfCF77DFB8(void); // EXECUTE
void mfE09A6F96(void); // STATE
void mf290C95CB(void); // ,
void mf1479C7E3(void); // _[LIT]
void mfBAFC44C4(void); // LITERAL
void mf26F8E50E(void); // _LITERAL?
void mf64EE01EC(void); // ___LITERAL?
void mf87601DFB(void); // _INTERPRET
void mf9FC8664E(void); // _EXCSRC
void mf93D1E229(void); // _EXCMSG
void mfEAF28AAC(void); // CATCH
void mf773B319E(void); // _THROW
void mf319DE64A(void); // EVALUATE
void mfE0018F77(void); // _STACKS
void mf2C72BE34(void); // _PROMPT
void mf84D9BC36(void); // QUIT
void mf28B3832A(void); // _BOOT
void mf49997C51(void); // _LOGO
void mf946BA378(void); // _ABORT
void mf367DE7D9(void); // ABORT
void mfB6A8BBC3(void); // BYE
void mf8A823C60(void); // _>LINK
void mf4B2037E9(void); // _>PFA
void mf03D7EB73(void); // _>NAME
void mf74404CD4(void); // _>CFA
void mfC439E65E(void); // _<CFA
void mfE345B847(void); // _CURRENT
void mfB8F49A42(void); // _LAST
void mfEA08BD65(void); // _LATEST
void mfEF0255CF(void); // WARNING
void mf20B9677F(void); // _HEADER
void mf3BFC9593(void); // _REVEAL
void mf779E612E(void); // _HIDE
void mfA483CD31(void); // __FLG!
void mfD1E3DADA(void); // IMMEDIATE
void mf72FB7883(void); // COMPILE-ONLY
void mfDE0C1FBA(void); // [
void mfD80C1648(void); // ]
void mfB988A2D6(void); // _[:]
void mf04316999(void); // __CSYS
void mf3FC95052(void); // __CSYS?
void mf3F0CB86D(void); // :
void mf3E0CB6DA(void); // ;
void mfF0A95069(void); // :NONAME
void mfBA444C0E(void); // [']
void mfD89985D6(void); // COMPILE,
void mfA66C8E4A(void); // [COMPILE]
void mf33530D01(void); // _POST[
void mf9A7AC789(void); // _]PONE
void mfDD00FE9F(void); // POSTPONE
void mfFDFF8019(void); // [CHAR]
void mfA2DA587A(void); // C,
void mf7BDCDF68(void); // _[SLIT]
void mf50BE3701(void); // SLITERAL
void mfF501E900(void); // S"
void mf94DA4270(void); // C"
void mf97D49429(void); // ."
void mfAC2233D4(void); // __PARSE|"
void mf853A39E8(void); // S|"
void mfFF7226AE(void); // _ABORT"
void mfC3343021(void); // ABORT"
void mf79836105(void); // EXIT
void mf992CB91D(void); // _[JMP]
void mf6156569F(void); // _[JMPZ]
void mf9592F3FF(void); // _MARK
void mf76D47BF1(void); // _>MARK
void mf79E02667(void); // _<MARK
void mf58E8EE86(void); // IF
void mf69F37330(void); // ELSE
void mfE78A4DB6(void); // THEN
void mfD8776DB1(void); // CASE
void mf88E4BCE8(void); // OF
void mfB2F63B9B(void); // ENDOF
void mf75E9FDF6(void); // ENDCASE
void mf2AEDCB62(void); // RECURSE
void mf3F126D5E(void); // BEGIN
void mf9A02B1AE(void); // WHILE
void mf6588C8EA(void); // REPEAT
void mf64F9C7EF(void); // UNTIL
void mf5E7C87EF(void); // AGAIN
void mfDA088408(void); // _[LOOP]
void mfC0946685(void); // _[DO]
void mfE2A5025C(void); // _[?DO]
void mf34D794DE(void); // __DO
void mf41CE86D4(void); // DO
void mfEC084DA3(void); // ?DO
void mfCC0C0364(void); // I
void mfCF0C081D(void); // J
void mfE11A6788(void); // UNLOOP
void mf354C36E0(void); // LEAVE
void mfF033FF78(void); // +LOOP
void mfDBE07C6B(void); // LOOP
void mf926854F5(void); // _[LOOP]_
void mf039DE92E(void); // _[DO]_
void mfCCC22AB9(void); // _[?DO]_
void mfB05EC713(void); // __DO_
void mf231DCCD1(void); // DO_
void mf8D12C3B4(void); // ?DO_
void mf71E915E1(void); // I_
void mf31F0FFE6(void); // J_
void mf3CF98E2B(void); // _[VAR]
void mf04B69205(void); // VARIABLE
void mf5047FCD5(void); // _[CONST]
void mfADA9DCA5(void); // CONSTANT
void mfCD566A47(void); // _[BUF]
void mf86AC1293(void); // BUFFER:
void mfCDFDC67E(void); // _[CREATE]
void mfC19230DD(void); // CREATE
void mf4520460B(void); // _<DOES
void mfFF60370A(void); // DOES>
void mf800A2691(void); // >BODY
void mf9F67357B(void); // _>V+
void mf99672C09(void); // _>V!
void mf38669356(void); // _>V@
void mf3C6699A2(void); // _>VD
void mf80B951E1(void); // _[VAL]
void mf24D3F916(void); // _(VALUE)
void mf2513E22A(void); // VALUE
void mfCCCCC70B(void); // __?VAL
void mf21F563E4(void); // TO
void mfBA65A38C(void); // _[DEFER]
void mfDC2E9701(void); // DEFER@
void mf3B2F2C8E(void); // DEFER!
void mfE9EDB1DB(void); // DEFER
void mfCB746F0B(void); // __?DEF
void mf55EA9727(void); // ACTION-OF
void mf6DE90F95(void); // IS
void mf55174F30(void); // _[MARKER]
void mfB53F8A17(void); // MARKER
void mf304F16BF(void); // ENVIRONMENT?
void mf9A18E8D5(void); // FLOAD
void mfC6897287(void); // DUMP
void mf07F5E02A(void); // WORDS
void mfCDA5B50C(void); // _STDS
void mfDBA5CB16(void); // _STDE
void mf5BF2539C(void); // _VERBOSE
void mf1E9174EB(void); // _ATB
void mfF959CA7B(void); // TESTING
void mf4DF5A928(void); // T{
void mf11CD0572(void); // ->
void mf4DC062EA(void); // __}T
void mf7756E4B4(void); // }T
void mf8E9972D8(void); // _TICKER
void mf96116CF3(void); // _TMFL
void mf889600AF(void); // _TIMER
void mfFAA3E0E8(void); // TIMER-RESET
void mf2F77738D(void); // TIMER-STOP
void mfC3A5A6AF(void); // .ELAPSED
void mf96272888(void); // MAIN

// --- Forth Dictionary ---

mfHdr mfdict[309]={{NULL,0,NULL,NULL},
{mfdict,0,"\005THROW",mfE5B4B40F},
{mfdict,0,"\004_RP!",mf8B51E6F7},
{mfdict+1,0,"\002>R",mf67FCEB09},
{mfdict+3,0,"\002R>",mf130456D1},
{mfdict+4,0,"\002R@",mf7D04FDAF},
{mfdict,0,"\004_SP!",mf89C518BC},
{mfdict+5,0,"\005DEPTH",mfCE61558A},
{mfdict+7,0,"\004DROP",mf52C16B0C},
{mfdict+8,0,"\004SWAP",mfA2DF200E},
{mfdict+9,0,"\003ROT",mf87E8362E},
{mfdict+10,0,"\003DUP",mf57E80646},
{mfdict+11,0,"\004OVER",mf2FE7860F},
{mfdict+12,0,"\004?DUP",mf8003A4B3},
{mfdict+13,0,"\004PICK",mfE67669F8},
{mfdict+14,0,"\003NIP",mf63DECBC8},
{mfdict+15,0,"\004TUCK",mf93A82DE8},
{mfdict+16,0,"\004ROLL",mf5451721E},
{mfdict+17,0,"\0052DROP",mfEF6476DC},
{mfdict+18,0,"\0042DUP",mfE0839FF6},
{mfdict+19,0,"\0052OVER",mf8C8BC01F},
{mfdict+20,0,"\0052SWAP",mf404D123E},
{mfdict+21,0,"\0032>R",mf89F60699},
{mfdict+22,0,"\0032R>",mf34FF0561},
{mfdict+23,0,"\0032R@",mf3EFF151F},
{mfdict+24,0,"\004NOOP",mfAC2CA76D},
{mfdict+25,0,"\004TRUE",mf501B0925},
{mfdict+26,0,"\005FALSE",mfEE597878},
{mfdict,0,"\004_MSB",mf6B09DC74},
{mfdict+27,0,"\003AND",mf91666DC6},
{mfdict+29,0,"\002OR",mf7CE4AA04},
{mfdict+30,0,"\003XOR",mf4F46575E},
{mfdict+31,0,"\006INVERT",mfD745D921},
{mfdict+32,0,"\006LSHIFT",mf84439159},
{mfdict+33,0,"\006RSHIFT",mf24728B77},
{mfdict+34,0,"\0020=",mf14ED5DD6},
{mfdict+35,0,"\0020<",mf15ED5F69},
{mfdict+36,0,"\0020>",mf13ED5C43},
{mfdict+37,0,"\0030<>",mfDBAD15F5},
{mfdict+38,0,"\001=",mf380CAD68},
{mfdict+39,0,"\001<",mf390CAEFB},
{mfdict+40,0,"\002U<",mf0EF30764},
{mfdict+41,0,"\001>",mf3B0CB221},
{mfdict+42,0,"\002<>",mf93F7201F},
{mfdict+43,0,"\002U>",mf10F30A8A},
{mfdict+44,0,"\001+",mf2E0C9DAA},
{mfdict+45,0,"\001-",mf280C9438},
{mfdict+46,0,"\006NEGATE",mf5EE442DB},
{mfdict+47,0,"\0021+",mf26EB3B95},
{mfdict+48,0,"\0021-",mf20EB3223},
{mfdict+49,0,"\003ABS",mf6C84489B},
{mfdict+50,0,"\001*",mf2F0C9F3D},
{mfdict+51,0,"\004/MOD",mf45BE23A2},
{mfdict+52,0,"\001/",mf2A0C975E},
{mfdict+53,0,"\003MOD",mf6453F3A3},
{mfdict+54,0,"\0022*",mfA7F2C26D},
{mfdict+55,0,"\0022/",mfA2F2BA8E},
{mfdict+56,0,"\006WITHIN",mf09D242EA},
{mfdict+57,0,"\003MAX",mf1C599279},
{mfdict+58,0,"\003MIN",mf0E45F4B7},
{mfdict,0,"\004_MU+",mfE40F181D},
{mfdict,0,"\004_MU*",mfE30F168A},
{mfdict,0,"\007_MU/MOD",mf1691D15F},
{mfdict+59,0,"\003UM*",mfD9ACE697},
{mfdict+63,0,"\006UM/MOD",mf5F2279EC},
{mfdict+64,0,"\003S>D",mf16478CF0},
{mfdict+65,0,"\007DNEGATE",mf8B105859},
{mfdict+66,0,"\004DABS",mfE23549BD},
{mfdict+67,0,"\002M*",mf1CDF95B6},
{mfdict+68,0,"\006SM/REM",mf1705E412},
{mfdict+69,0,"\006FM/MOD",mf66F1FDC7},
{mfdict+70,0,"\005*/MOD",mfE5A71F0A},
{mfdict+71,0,"\002*/",mf22DE6956},
{mfdict+72,0,"\005CHAR+",mf2FEE4BC2},
{mfdict+73,0,"\005CHARS",mfC7EDA80A},
{mfdict+74,0,"\002C@",mf76DA1336},
{mfdict+75,0,"\002C!",mf97DA4729},
{mfdict+76,0,"\004MOVE",mfC2064154},
{mfdict+77,0,"\004FILL",mfAF3DC588},
{mfdict+78,0,"\005ERASE",mfB7F60EB5},
{mfdict+79,0,"\005CELL+",mfBB8FCA7A},
{mfdict+80,0,"\005CELLS",mf838F7252},
{mfdict+81,0,"\001@",mfC50BF85F},
{mfdict+82,0,"\001!",mf240C8DEC},
{mfdict+83,0,"\002+!",mf08DC01D1},
{mfdict+84,0,"\0022@",mf3DF21B8F},
{mfdict+85,0,"\0022!",mf9CF2B11C},
{mfdict+86,0,"\005COUNT",mfA3F7B2D4},
{mfdict+87,0,"\007/STRING",mf62A404AF},
{mfdict+88,0,"\005CHAR/",mf2BEE4576},
{mfdict+89,0,"\002S=",mf140219CD},
{mfdict,0,"\006_UPPER",mfEE2653C2},
{mfdict,0,"\006_PLACE",mf10E38971},
{mfdict,0,"\007_ORIGIN",mf12E2D898},
{mfdict,0,"\006_LIMIT",mf9F4C117D},
{mfdict,0,"\003_DP",mfD148A1CA},
{mfdict+90,0,"\004HERE",mf5F2B6B8B},
{mfdict+96,0,"\005ALLOT",mf3FA1943F},
{mfdict+97,0,"\007ALIGNED",mf901B2AFF},
{mfdict+98,0,"\005ALIGN",mfAFA183BE},
{mfdict+99,0,"\003PAD",mf9E0B1B0A},
{mfdict+100,0,"\006UNUSED",mfCF430E1B},
{mfdict+101,0,"\004BASE",mfE972DB58},
{mfdict+102,0,"\007DECIMAL",mf48AF9A2C},
{mfdict+103,0,"\003HEX",mf818F192A},
{mfdict,0,"\010_DIGIT>N",mf7A717643},
{mfdict+104,0,"\007>NUMBER",mf5773A68C},
{mfdict+106,0,"\007NUMBER?",mf867FFF4D},
{mfdict+107,0,"\004EMIT",mfD9BC5E8A},
{mfdict+108,0,"\002BL",mf20DD5D6B},
{mfdict+109,0,"\005SPACE",mf1808F0E5},
{mfdict+110,0,"\006SPACES",mf8C12EE82},
{mfdict+111,0,"\002CR",mf64D9F6E0},
{mfdict+112,0,"\004TYPE",mf142FE78D},
{mfdict,0,"\010_N>DIGIT",mf6B388877},
{mfdict+113,0,"\004HOLD",mfEBB64F78},
{mfdict+115,0,"\005HOLDS",mf3AFEA0B1},
{mfdict+116,0,"\002<#",mfA6F73E08},
{mfdict+117,0,"\001#",mf260C9112},
{mfdict+118,0,"\002#>",mf11C88844},
{mfdict+119,0,"\002#S",mf26C8A953},
{mfdict+120,0,"\004SIGN",mf0AADBFA4},
{mfdict,0,"\004_(.)",mfE4E51507},
{mfdict+121,0,"\001.",mf2B0C98F1},
{mfdict+123,0,"\002.R",mf67D44899},
{mfdict,0,"\005_(U.)",mfFD90D4E8},
{mfdict+124,0,"\002U.",mf20F323BA},
{mfdict+126,0,"\003U.R",mfC6C18638},
{mfdict+127,0,"\003KEY",mfACCAE4CC},
{mfdict+128,0,"\006ACCEPT",mf209E17E9},
{mfdict,0,"\004_#IB",mf0119CBC8},
{mfdict,0,"\004_TIB",mfC49B66C5},
{mfdict,0,"\005_SIBS",mfB3D82B11},
{mfdict,0,"\004_SIB",mf12AE9718},
{mfdict,0,"\005_>SIB",mfCFEC5BB6},
{mfdict,0,"\010_SRCSPEC",mf01A69217},
{mfdict,0,"\007_SRCLEN",mf7C1EEF6B},
{mfdict,0,"\007_SRCADR",mf7A91D589},
{mfdict,0,"\007_SRCID!",mfFD898658},
{mfdict,0,"\006_SRC2!",mf5AF98C79},
{mfdict,0,"\006_SRC>R",mf600421B4},
{mfdict,0,"\006_R>SRC",mfD24E2A14},
{mfdict+129,0,"\006SOURCE",mfB9790298},
{mfdict+142,0,"\003>IN",mf580B1E42},
{mfdict+143,0,"\011SOURCE-ID",mf254113A2},
{mfdict,0,"\004_BLK",mfEF18D029},
{mfdict+144,0,"\012SAVE-INPUT",mf94A8FFED},
{mfdict+146,0,"\015RESTORE-INPUT",mfA10A7AB8},
{mfdict+147,0,"\006REFILL",mfC1D4E711},
{mfdict,0,"\005_SKIP",mfD66336C5},
{mfdict,0,"\005_SCAN",mfC119E57B},
{mfdict,0,"\006_PARSE",mfADFC5551},
{mfdict+148,0,"\005PARSE",mfD918C10C},
{mfdict,0,"\013_PARSE-CHAR",mfF08832C0},
{mfdict,0,"\013_PARSE-NAME",mfE9BCE12F},
{mfdict+152,0,"\012PARSE-NAME",mfF92179F8},
{mfdict,0,"\007_PARSED",mfF939F00F},
{mfdict+155,0,"\004CAPS",mfA274DA18},
{mfdict+157,0,"\004WORD",mf6D01E13D},
{mfdict,0,"\013_PARSE-WORD",mfC94E167C},
{mfdict+158,0,"\004CHAR",mfA59F665D},
{mfdict+160,1,"\001\\",mfD90C17DB},
{mfdict+161,1,"\001(",mf2D0C9C17},
{mfdict+162,1,"\002.(",mf9DD49D9B},
{mfdict,0,"\005_FIND",mf0E9A8AA9},
{mfdict,0,"\012_WORDLISTS",mf85A4C44F},
{mfdict,0,"\010_CONTEXT",mf82CE5579},
{mfdict,0,"\012_FIND-WORD",mf5AADB2E4},
{mfdict+163,0,"\004FIND",mfFB42BA5A},
{mfdict+168,0,"\001\'",mf220C8AC6},
{mfdict+169,0,"\007EXECUTE",mfCF77DFB8},
{mfdict+170,0,"\005STATE",mfE09A6F96},
{mfdict+171,0,"\001,",mf290C95CB},
{mfdict,0,"\006_[LIT]",mf1479C7E3},
{mfdict+172,3,"\007LITERAL",mfBAFC44C4},
{mfdict,0,"\011_LITERAL?",mf26F8E50E},
{mfdict,0,"\012_INTERPRET",mf87601DFB},
{mfdict,0,"\007_EXCSRC",mf9FC8664E},
{mfdict,0,"\007_EXCMSG",mf93D1E229},
{mfdict+174,0,"\005CATCH",mfEAF28AAC},
{mfdict,0,"\006_THROW",mf773B319E},
{mfdict+179,0,"\010EVALUATE",mf319DE64A},
{mfdict,0,"\007_STACKS",mfE0018F77},
{mfdict,0,"\007_PROMPT",mf2C72BE34},
{mfdict+181,0,"\004QUIT",mf84D9BC36},
{mfdict,0,"\005_BOOT",mf28B3832A},
{mfdict,0,"\005_LOGO",mf49997C51},
{mfdict,0,"\006_ABORT",mf946BA378},
{mfdict+184,0,"\005ABORT",mf367DE7D9},
{mfdict+188,0,"\003BYE",mfB6A8BBC3},
{mfdict,0,"\006_>LINK",mf8A823C60},
{mfdict,0,"\005_>PFA",mf4B2037E9},
{mfdict,0,"\006_>NAME",mf03D7EB73},
{mfdict,0,"\005_>CFA",mf74404CD4},
{mfdict,0,"\005_<CFA",mfC439E65E},
{mfdict,0,"\010_CURRENT",mfE345B847},
{mfdict,0,"\005_LAST",mfB8F49A42},
{mfdict,0,"\007_LATEST",mfEA08BD65},
{mfdict+189,0,"\007WARNING",mfEF0255CF},
{mfdict,0,"\007_HEADER",mf20B9677F},
{mfdict,0,"\007_REVEAL",mf3BFC9593},
{mfdict,0,"\005_HIDE",mf779E612E},
{mfdict+198,0,"\011IMMEDIATE",mfD1E3DADA},
{mfdict+202,0,"\014COMPILE-ONLY",mf72FB7883},
{mfdict+203,1,"\001[",mfDE0C1FBA},
{mfdict+204,0,"\001]",mfD80C1648},
{mfdict,0,"\004_[:]",mfB988A2D6},
{mfdict+205,0,"\001:",mf3F0CB86D},
{mfdict+207,3,"\001;",mf3E0CB6DA},
{mfdict+208,0,"\007:NONAME",mfF0A95069},
{mfdict+209,3,"\003[\']",mfBA444C0E},
{mfdict+210,0,"\010COMPILE,",mfD89985D6},
{mfdict+211,3,"\011[COMPILE]",mfA66C8E4A},
{mfdict,0,"\006_POST[",mf33530D01},
{mfdict,0,"\006_]PONE",mf9A7AC789},
{mfdict+212,3,"\010POSTPONE",mfDD00FE9F},
{mfdict+215,3,"\006[CHAR]",mfFDFF8019},
{mfdict+216,0,"\002C,",mfA2DA587A},
{mfdict,0,"\007_[SLIT]",mf7BDCDF68},
{mfdict+217,3,"\010SLITERAL",mf50BE3701},
{mfdict+219,1,"\002S\"",mfF501E900},
{mfdict+220,3,"\002C\"",mf94DA4270},
{mfdict+221,1,"\002.\"",mf97D49429},
{mfdict+222,1,"\003S\\\"",mf853A39E8},
{mfdict,0,"\007_ABORT\"",mfFF7226AE},
{mfdict+223,3,"\006ABORT\"",mfC3343021},
{mfdict+225,3,"\004EXIT",mf79836105},
{mfdict,0,"\006_[JMP]",mf992CB91D},
{mfdict,0,"\007_[JMPZ]",mf6156569F},
{mfdict,0,"\005_MARK",mf9592F3FF},
{mfdict,0,"\006_>MARK",mf76D47BF1},
{mfdict,0,"\006_<MARK",mf79E02667},
{mfdict+226,3,"\002IF",mf58E8EE86},
{mfdict+232,3,"\004ELSE",mf69F37330},
{mfdict+233,3,"\004THEN",mfE78A4DB6},
{mfdict+234,3,"\004CASE",mfD8776DB1},
{mfdict+235,3,"\002OF",mf88E4BCE8},
{mfdict+236,3,"\005ENDOF",mfB2F63B9B},
{mfdict+237,3,"\007ENDCASE",mf75E9FDF6},
{mfdict+238,3,"\007RECURSE",mf2AEDCB62},
{mfdict+239,3,"\005BEGIN",mf3F126D5E},
{mfdict+240,3,"\005WHILE",mf9A02B1AE},
{mfdict+241,3,"\006REPEAT",mf6588C8EA},
{mfdict+242,3,"\005UNTIL",mf64F9C7EF},
{mfdict+243,3,"\005AGAIN",mf5E7C87EF},
{mfdict,0,"\007_[LOOP]",mfDA088408},
{mfdict,0,"\005_[DO]",mfC0946685},
{mfdict,0,"\006_[?DO]",mfE2A5025C},
{mfdict+244,3,"\002DO",mf41CE86D4},
{mfdict+248,3,"\003?DO",mfEC084DA3},
{mfdict+249,2,"\001I",mfCC0C0364},
{mfdict+250,2,"\001J",mfCF0C081D},
{mfdict+251,2,"\006UNLOOP",mfE11A6788},
{mfdict+252,3,"\005LEAVE",mf354C36E0},
{mfdict+253,3,"\005+LOOP",mfF033FF78},
{mfdict+254,3,"\004LOOP",mfDBE07C6B},
{mfdict,0,"\010_[LOOP]_",mf926854F5},
{mfdict,0,"\006_[DO]_",mf039DE92E},
{mfdict,0,"\007_[?DO]_",mfCCC22AB9},
{mfdict+255,3,"\003DO_",mf231DCCD1},
{mfdict+259,3,"\004?DO_",mf8D12C3B4},
{mfdict+260,2,"\002I_",mf71E915E1},
{mfdict+261,2,"\002J_",mf31F0FFE6},
{mfdict,0,"\006_[VAR]",mf3CF98E2B},
{mfdict+262,0,"\010VARIABLE",mf04B69205},
{mfdict,0,"\010_[CONST]",mf5047FCD5},
{mfdict+264,0,"\010CONSTANT",mfADA9DCA5},
{mfdict,0,"\006_[BUF]",mfCD566A47},
{mfdict+266,0,"\007BUFFER:",mf86AC1293},
{mfdict,0,"\011_[CREATE]",mfCDFDC67E},
{mfdict+268,0,"\006CREATE",mfC19230DD},
{mfdict,0,"\006_<DOES",mf4520460B},
{mfdict+270,3,"\005DOES>",mfFF60370A},
{mfdict+272,0,"\005>BODY",mf800A2691},
{mfdict,0,"\004_>V+",mf9F67357B},
{mfdict,0,"\004_>V!",mf99672C09},
{mfdict,0,"\004_>V@",mf38669356},
{mfdict,0,"\004_>VD",mf3C6699A2},
{mfdict,0,"\006_[VAL]",mf80B951E1},
{mfdict,0,"\010_(VALUE)",mf24D3F916},
{mfdict+273,0,"\005VALUE",mf2513E22A},
{mfdict+280,1,"\002TO",mf21F563E4},
{mfdict,0,"\010_[DEFER]",mfBA65A38C},
{mfdict+281,0,"\006DEFER@",mfDC2E9701},
{mfdict+283,0,"\006DEFER!",mf3B2F2C8E},
{mfdict+284,0,"\005DEFER",mfE9EDB1DB},
{mfdict+285,1,"\011ACTION-OF",mf55EA9727},
{mfdict+286,1,"\002IS",mf6DE90F95},
{mfdict,0,"\011_[MARKER]",mf55174F30},
{mfdict+287,0,"\006MARKER",mfB53F8A17},
{mfdict+289,0,"\014ENVIRONMENT?",mf304F16BF},
{mfdict+290,0,"\005FLOAD",mf9A18E8D5},
{mfdict+291,0,"\004DUMP",mfC6897287},
{mfdict+292,0,"\005WORDS",mf07F5E02A},
{mfdict,0,"\005_STDS",mfCDA5B50C},
{mfdict,0,"\005_STDE",mfDBA5CB16},
{mfdict,0,"\010_VERBOSE",mf5BF2539C},
{mfdict,0,"\004_ATB",mf1E9174EB},
{mfdict+293,0,"\007TESTING",mfF959CA7B},
{mfdict+298,0,"\002T{",mf4DF5A928},
{mfdict+299,0,"\002->",mf11CD0572},
{mfdict+300,0,"\002}T",mf7756E4B4},
{mfdict,0,"\007_TICKER",mf8E9972D8},
{mfdict,0,"\005_TMFL",mf96116CF3},
{mfdict,0,"\006_TIMER",mf889600AF},
{mfdict+301,0,"\013TIMER-RESET",mfFAA3E0E8},
{mfdict+305,0,"\012TIMER-STOP",mf2F77738D},
{mfdict+306,0,"\010.ELAPSED",mfC3A5A6AF},
{mfdict+307,0,"\004MAIN",mf96272888}};

// --- Search Parameters

#define MFPRIMS 308
void* MFLAST=mfdict+MFPRIMS;
void* MFLATEST=mfdict+MFPRIMS;
void* MFFWL[20]={mfdict+MFPRIMS,"\005FORTH",NULL};
void* MFCTX[8]={MFFWL,NULL};

// --- Forth Definitions

#define MFCORE 

mfXT MFE5B4B40F=mf773B319E; // IS _THROW

void mfE5B4B40F(void) { // DEFER THROW
  (MFE5B4B40F)();
}

void mf8B51E6F7(void) { // : _RP!
  mferr(mfin(1);mfwithin(0,mftos,MFRSTSIZE)); 
  mfrp=mfrst+mfpop();
}

static inline void mf67FCEB09(void) { // : >R
  mferr(mfin(1);mfrout(1)); 
  mfrpush(mfpop());
}

static inline void mf130456D1(void) { // : R>
  mferr(mfrin(1);mfout(1)); 
  mfpush(mfrpop());
}

static inline void mf7D04FDAF(void) { // : R@
  mferr(mfrin(1);mfout(1)); 
  mfpush(mfrtos);
}

void mf89C518BC(void) { // : _SP!
  mferr(mfin(1);mfwithin(0,mftos,MFSTKSIZE)); 
  mfsp=mfstk+mftos;
}

void mfCE61558A(void) { // : DEPTH
  mferr(mfout(1)); 
  mfCell u=mfsp-mfstk; 
  mfpush(u);
}

static inline void mf52C16B0C(void) { // : DROP
  mferr(mfin(1)); 
  mfdrop;
}

static inline void mfA2DF200E(void) { // : SWAP
  mferr(mfin(2)); 
  mfCell a=mfsec; 
  mfsec=mftos, mftos=a;
}

static inline void mf87E8362E(void) { // : ROT
  mferr(mfin(3)); 
  mfCell a=mfthd; 
  mfthd=mfsec,mfsec=mftos,mftos=a;
}

static inline void mf57E80646(void) { // : DUP
  mferr(mfin(1);mfout(1)); 
  mfup, mftos=mfsec;
}

static inline void mf2FE7860F(void) { // : OVER
  mferr(mfin(2);mfout(1)); 
  mfup, mftos=mfthd;
}

static inline void mf8003A4B3(void) { // : ?DUP
  if(mftos)
  mf57E80646(); // DUP
}

static inline void mfE67669F8(void) { // : PICK
  mferr(mfin(mftos+1)); 
  mftos=mfsp[-mftos-1];
}

static inline void mf63DECBC8(void) { // : NIP
  mferr(mfin(2)); 
  mfsec=mftos, mfdrop;
}

static inline void mf93A82DE8(void) { // : TUCK
  mferr(mfin(2);mfout(1)); 
  mfup, mftos=mfsec, mfsec=mfthd, mfthd=mftos;
}

void mf5451721E(void) { // : ROLL
  mferr(mfin(mftos+2)); 
  mfCell n=-mfpop(); mfCell x=mfsp[n]; 
  while(n<0) { mfsp[n]=mfsp[n+1]; n++; } 
  mftos=x;
}

static inline void mfEF6476DC(void) { // : 2DROP
  mferr(mfin(2)); 
  mf2drop;
}

static inline void mfE0839FF6(void) { // : 2DUP
  mferr(mfin(2);mfout(2)); 
  mfCell a=mfsec, b=mftos; 
  mfpush(a), mfpush(b);
}

static inline void mf8C8BC01F(void) { // : 2OVER
  mferr(mfin(4);mfout(2)); 
  mfCell a=mffth, b=mfthd; 
  mfpush(a), mfpush(b);
}

static inline void mf404D123E(void) { // : 2SWAP
  mferr(mfin(4)); 
  mfCell a=mffth, b=mfthd, c=mfsec, d=mftos; 
  mffth=c, mfthd=d, mfsec=a, mftos=b;
}

static inline void mf89F60699(void) { // : 2>R
  mferr(mfin(2);mfrout(2)); 
  mfCell x=mfpop(); 
  mfrpush(mfpop()), mfrpush(x);
}

static inline void mf34FF0561(void) { // : 2R>
  mferr(mfrin(2);mfout(2)); 
  mfCell x=mfrpop(); 
  mfpush(mfrpop()), mfpush(x);
}

static inline void mf3EFF151F(void) { // : 2R@
  mferr(mfrin(2);mfout(2)); 
  mfpush(mfrsec), mfpush(mfrtos);
}

void mfAC2CA76D(void) { // : NOOP
}

const mfCell MF501B0925=-1; // -1

void mf501B0925(void) { // CONSTANT TRUE
  mfpush(MF501B0925);
}

const mfCell MFEE597878=0; // 0

void mfEE597878(void) { // CONSTANT FALSE
  mfpush(MFEE597878);
}

static inline void mf6B09DC74(void) { // : _MSB
  mferr(mfout(1)); 
  mfpush(~INTPTR_MAX);
}

static inline void mf91666DC6(void) { // : AND
  mferr(mfin(2)); 
  mfdrop, mftos&=mfontos;
}

static inline void mf7CE4AA04(void) { // : OR
  mferr(mfin(2)); 
  mfdrop, mftos|=mfontos;
}

static inline void mf4F46575E(void) { // : XOR
  mferr(mfin(2)); 
  mfdrop, mftos^=mfontos;
}

static inline void mfD745D921(void) { // : INVERT
  mferr(mfin(1)); 
  mftos=~mftos;
}

static inline void mf84439159(void) { // : LSHIFT
  mferr(mfin(2)); 
  mfCell p=mfpop(); mftos<<=p;
}

static inline void mf24728B77(void) { // : RSHIFT
  mferr(mfin(2)); 
  mfCell p=mfpop(); mftos=(mfUCell)mftos>>p;
}

static inline void mf14ED5DD6(void) { // : 0=
  mferr(mfin(1)); 
  mftos=-(mftos==0);
}

static inline void mf15ED5F69(void) { // : 0<
  mferr(mfin(1)); 
  mftos=-(mftos<0);
}

static inline void mf13ED5C43(void) { // : 0>
  mferr(mfin(1)); 
  mftos=-(mftos>0);
}

static inline void mfDBAD15F5(void) { // : 0<>
  mferr(mfin(1)); 
  mftos=-(mftos!=0);
}

static inline void mf380CAD68(void) { // : =
  mferr(mfin(2)); 
  mfdrop, mftos=-(mftos==mfontos);
}

static inline void mf390CAEFB(void) { // : <
  mferr(mfin(2)); 
  mfdrop, mftos=-(mftos<mfontos);
}

static inline void mf0EF30764(void) { // : U<
  mferr(mfin(2)); 
  mfdrop, mftos=-((mfUCell)mftos<(mfUCell)mfontos);
}

static inline void mf3B0CB221(void) { // : >
  mferr(mfin(2)); 
  mfdrop, mftos=-(mftos>mfontos);
}

static inline void mf93F7201F(void) { // : <>
  mferr(mfin(2)); 
  mfdrop, mftos=-(mftos!=mfontos);
}

static inline void mf10F30A8A(void) { // : U>
  mferr(mfin(2)); 
  mfdrop, mftos=-((mfUCell)mftos>(mfUCell)mfontos);
}

static inline void mf2E0C9DAA(void) { // : +
  mferr(mfin(2)); 
  mfdrop, mftos+=mfontos;
}

static inline void mf280C9438(void) { // : -
  mferr(mfin(2)); 
  mfdrop, mftos-=mfontos;
}

static inline void mf5EE442DB(void) { // : NEGATE
  mferr(mfin(1)); 
  mftos=-mftos;
}

static inline void mf26EB3B95(void) { // : 1+
  mferr(mfin(1)); 
  mftos+=1;
}

static inline void mf20EB3223(void) { // : 1-
  mferr(mfin(1)); 
  mftos-=1;
}

static inline void mf6C84489B(void) { // : ABS
  mferr(mfin(1)); 
  mftos=llabs(mftos);
}

static inline void mf2F0C9F3D(void) { // : *
  mferr(mfin(2)); 
  mfdrop, mftos*=mfontos;
}

void mf45BE23A2(void) { // : /MOD
  mferr(mfin(2);mfzero(mftos)); 
  mfCell a=mfsec, b=mftos, q=a/b, r=a-q*b; 
  mfsec=r, mftos=q;
}

static inline void mf2A0C975E(void) { // : /
  mferr(mfin(2);mfzero(mftos)); 
  mfdrop, mftos/=mfontos;
}

static inline void mf6453F3A3(void) { // : MOD
  mferr(mfin(2);mfzero(mftos)); 
  mfdrop, mftos%=mfontos;
}

static inline void mfA7F2C26D(void) { // : 2*
  mferr(mfin(1)); 
  mftos<<=1;
}

static inline void mfA2F2BA8E(void) { // : 2/
  mferr(mfin(1)); 
  mftos>>=1;
}

void mf09D242EA(void) { // : WITHIN
  mf2FE7860F(); // OVER
  mf280C9438(); // -
  mf67FCEB09(); // >R
  mf280C9438(); // -
  mf130456D1(); // R>
  mf0EF30764(); // U<
}

void mf1C599279(void) { // : MAX
  mfE0839FF6(); // 2DUP
  mf390CAEFB(); // <
mfIF
  mfA2DF200E(); // SWAP
mfTHEN
  mf52C16B0C(); // DROP
}

void mf0E45F4B7(void) { // : MIN
  mfE0839FF6(); // 2DUP
  mf390CAEFB(); // <
mfNOTIF
  mfA2DF200E(); // SWAP
mfTHEN
  mf52C16B0C(); // DROP
}

void mfE40F181D(void) { // : _MU+
  mferr(mfin(3)); 
#ifdef MFDBL 
  mfUCell w=mfpop(); mfUDbl ud=mfdtos; 
  mfdtos=ud+w; 
#else 
  mfUCell w=mfpop(), lo=mfsec+w; 
  mfsec=lo, mftos+=lo<w; 
#endif 
}

void mfE30F168A(void) { // : _MU*
  mferr(mfin(3)); 
#ifdef MFDBL 
  mfUCell w=mfpop(); mfUDbl ud=mfdtos; 
  mfdtos=ud*w; 
#else 
  mfUCell c=mfpop(), b=mftos, a=mfsec; 
  mfUCell cl=c&0xffffffff, ch=c>>32, al=a&0xffffffff, ah=a>>32; 
  mfsec = a*c, mftos = b*c + ah*ch; 
  a = ch*al, b = a + cl*ah + ((cl*al)>>32), c = b>>32; 
  mftos += (a<=b ? c : c|0x100000000); 
#endif 
}

void mf1691D15F(void) { // : _MU/MOD
  mferr(mfin(3);mfzero(mftos)); 
#ifdef MFDBL 
  mfUDbl udq, ud=*(mfUDbl*)(mfsp-2); mfUCell v=mftos; 
  if(ud==v) mfthd=0, mfsec=1, mftos=0; 
  else mfdtos=udq=ud/v, mfthd=ud-udq*v; 
#else 
  mfUCell v=mftos, a, b=mfthd, c=mfsec, d; 
  if (!c&&(b==v)) a=1, b=c=0; else { 
    if (!c) a=b/v, c=b-a*v, b=0; else { 
      a=c/v, c-=a*v;   
      for (int i=0; i<64; i++) { 
        d=c>>63, c=(c<<1)|(b>>63), b=(b<<1)|(a>>63), a<<=1; 
        if ((c>=v)||d) c-=v, a+=1; } } } 
  mfsec=a, mftos=b, mfthd=c; 
#endif 
}

void mfD9ACE697(void) { // : UM*
  mfpush(0); // $0
  mfA2DF200E(); // SWAP
  mfE30F168A(); // _MU*
}

void mf5F2279EC(void) { // : UM/MOD
  mf1691D15F(); // _MU/MOD
  mf52C16B0C(); // DROP
}

void mf16478CF0(void) { // : S>D
  mf57E80646(); // DUP
  mf15ED5F69(); // 0<
}

void mf8B105859(void) { // : DNEGATE
  mferr(mfin(2)); 
#ifdef MFDBL 
  mfdtos=-mfdtos; 
#else 
  mfUCell l=~mfsec+1, h=~mftos+!l; 
  mfsec=l, mftos=h; 
#endif 
}

void mfE23549BD(void) { // : DABS
  mf57E80646(); // DUP
  mf15ED5F69(); // 0<
mfIF
  mf8B105859(); // DNEGATE
mfTHEN
}

void mf1CDF95B6(void) { // : M*
  mfE0839FF6(); // 2DUP
  mf4F46575E(); // XOR
  mf67FCEB09(); // >R
  mf6C84489B(); // ABS
  mfA2DF200E(); // SWAP
  mf6C84489B(); // ABS
  mfD9ACE697(); // UM*
  mf130456D1(); // R>
  mf15ED5F69(); // 0<
mfIF
  mf8B105859(); // DNEGATE
mfTHEN
}

void mf1705E412(void) { // : SM/REM
  mferr(mfin(3);mfzero(mftos)); 
  mfCell z=mfsec, n=mftos, s=(z^n)<0;   
  mf6C84489B(); // ABS
  mf67FCEB09(); // >R
  mfE23549BD(); // DABS
  mf130456D1(); // R>
  mf5F2279EC(); // UM/MOD
  if (s) mftos=-mftos;  
  if (z<0) mfsec=-mfsec;
}

void mf66F1FDC7(void) { // : FM/MOD
  mfCell n=mftos, s=(mfsec^n)<0; 
  mf1705E412(); // SM/REM
  if (s&&mfsec) mftos-=1, mfsec+=n;
}

void mfE5A71F0A(void) { // : */MOD
  mf67FCEB09(); // >R
  mf1CDF95B6(); // M*
  mf130456D1(); // R>
  mf1705E412(); // SM/REM
}

void mf22DE6956(void) { // : */
  mfE5A71F0A(); // */MOD
  mf63DECBC8(); // NIP
}

static inline void mf2FEE4BC2(void) { // : CHAR+
  mferr(mfin(1)); 
  mftos+=1;
}

void mfC7EDA80A(void) { // : CHARS
}

static inline void mf76DA1336(void) { // : C@
  mferr(mfin(1)); 
  mftos=mfcat(mftos);
}

static inline void mf97DA4729(void) { // : C!
  mferr(mfin(2)); 
  mfcat(mftos)=(mfChar)mfsec, mf2drop;
}

void mfC2064154(void) { // : MOVE
  mferr(mfin(3)); 
  if (mftos>0) memmove((char*)mfsec,(char*)mfthd,mftos); 
  mfsp-=3;
}

void mfAF3DC588(void) { // : FILL
  mferr(mfin(3)); 
  if (mfsec>0) memset((char*)mfthd,(char)mftos,mfsec); 
  mfsp-=3;
}

void mfB7F60EB5(void) { // : ERASE
  mfpush(0); // $0
  mfAF3DC588(); // FILL
}

static inline void mfBB8FCA7A(void) { // : CELL+
  mferr(mfin(1)); 
  mftos+=MFSIZE;
}

static inline void mf838F7252(void) { // : CELLS
  mferr(mfin(1)); 
  mftos*=MFSIZE;
}

static inline void mfC50BF85F(void) { // : @
  mferr(mfin(1)); 
  mftos=mfat(mftos);
}

static inline void mf240C8DEC(void) { // : !
  mferr(mfin(2)); 
  mfat(mftos)=mfsec, mf2drop;
}

static inline void mf08DC01D1(void) { // : +!
  mferr(mfin(2)); 
  mfat(mftos)+=mfsec, mf2drop;
}

static inline void mf3DF21B8F(void) { // : 2@
  mferr(mfin(1);mfout(1)); 
  mfCell a=mftos; 
  mftos=mfat(a+MFSIZE), mfpush(mfat(a));
}

static inline void mf9CF2B11C(void) { // : 2!
  mferr(mfin(3)); 
  mfCell a=mfpop(); 
  mfat(a)=mfpop(), mfat(a+MFSIZE)=mfpop();
}

void mfA3F7B2D4(void) { // : COUNT
  mf57E80646(); // DUP
  mf26EB3B95(); // 1+
  mfA2DF200E(); // SWAP
  mf76DA1336(); // C@
}

void mf62A404AF(void) { // : /STRING
  mf57E80646(); // DUP
  mf67FCEB09(); // >R
  mf280C9438(); // -
  mfA2DF200E(); // SWAP
  mf130456D1(); // R>
  mf2E0C9DAA(); // +
  mfA2DF200E(); // SWAP
}

void mf2BEE4576(void) { // : CHAR/
  mf20EB3223(); // 1-
  mfA2DF200E(); // SWAP
  mf26EB3B95(); // 1+
  mfA2DF200E(); // SWAP
}

void mf140219CD(void) { // : S=
  char *a1,*a2; int u1,u2; 
  u2=mfpop(), a2=(char*)mfpop(), u1=mfpop(), a1=(char*)mftos; 
  mftos = (u1==u2 ? -!memcmp(a1,a2,u1) : 0);
}

void mfEE2653C2(void) { // : _UPPER
  mfpush(1); // $1
mfFORUP
mfN
  mf76DA1336(); // C@
  mftos=toupper(mftos);
mfN
  mf97DA4729(); // C!
mfNEXT
}

void mf10E38971(void) { // : _PLACE
  mfA2DF200E(); // SWAP
  mfpush(254); // $FE
  mf0E45F4B7(); // MIN
  mfE0839FF6(); // 2DUP
  mfA2DF200E(); // SWAP
  mf97DA4729(); // C!
  mfA2DF200E(); // SWAP
  mf26EB3B95(); // 1+
  mfE0839FF6(); // 2DUP
  mf2E0C9DAA(); // +
  mfpush(0); // $0
  mfA2DF200E(); // SWAP
  mf97DA4729(); // C!
  mfA2DF200E(); // SWAP
  mfC2064154(); // MOVE
}

void mf12E2D898(void) { // : _ORIGIN
  mferr(mfout(1)); 
  mfpush(mfdsp);
}

void mf9F4C117D(void) { // : _LIMIT
  mferr(mfout(1)); 
  mfpush(mfdsp+MFDSPSIZE);
}

static inline void mfD148A1CA(void) { // : _DP
  mferr(mfout(1)); 
  mfpush(&mfdp);
}

static inline void mf5F2B6B8B(void) { // : HERE
  mfD148A1CA(); // _DP
  mfC50BF85F(); // @
}

void mf3FA1943F(void) { // : ALLOT
  mfD148A1CA(); // _DP
  mf08DC01D1(); // +!
}

void mf901B2AFF(void) { // : ALIGNED
  mferr(mfin(1)); 
  mftos=(mftos+MFSIZE-1)&(-MFSIZE);
}

void mfAFA183BE(void) { // : ALIGN
  mf5F2B6B8B(); // HERE
  mf901B2AFF(); // ALIGNED
  mfD148A1CA(); // _DP
  mf240C8DEC(); // !
}

void mf9E0B1B0A(void) { // : PAD
  mf5F2B6B8B(); // HERE
  mfpush(264); // $108
  mf2E0C9DAA(); // +
  mfpush(21); // $15
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
}

void mfCF430E1B(void) { // : UNUSED
  mf9F4C117D(); // _LIMIT
  mf9E0B1B0A(); // PAD
  mfpush(256); // $100
  mf2E0C9DAA(); // +
  mf280C9438(); // -
}

mfCell MFE972DB58=10; // 10

void mfE972DB58(void) { // VARIABLE BASE
  mfpush(&MFE972DB58);
}

static inline void mf48AF9A2C(void) { // : DECIMAL
  mfpush(10); // $A
  mfpush(&MFE972DB58); // BASE
  mf240C8DEC(); // !
}

static inline void mf818F192A(void) { // : HEX
  mfpush(16); // $10
  mfpush(&MFE972DB58); // BASE
  mf240C8DEC(); // !
}

void mf7A717643(void) { // : _DIGIT>N
  mfpush(&MFE972DB58); // BASE
  mfC50BF85F(); // @
  mfsec=mfdig2n(mfsec,mftos); mftos=(mfsec==256 ? 0 : -1);
}

void mf5773A68C(void) { // : >NUMBER
mfBEGIN
mfDUPWHILE
  mf2FE7860F(); // OVER
  mf76DA1336(); // C@
  mf7A717643(); // _DIGIT>N
mfNOTIF
  mf52C16B0C(); // DROP
mfEXIT
mfTHEN
  mf67FCEB09(); // >R
  mf404D123E(); // 2SWAP
  mf93A82DE8(); // TUCK
  mfpush(&MFE972DB58); // BASE
  mfC50BF85F(); // @
  mfE30F168A(); // _MU*
  mf130456D1(); // R>
  mfE40F181D(); // _MU+
  mf87E8362E(); // ROT
  mf2FE7860F(); // OVER
  mf10F30A8A(); // U>
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(-1); // $FFFFFFFF
  mfpush(-1); // $FFFFFFFF
  mf404D123E(); // 2SWAP
mfEXIT
mfTHEN
  mf404D123E(); // 2SWAP
  mf2BEE4576(); // CHAR/
mfREPEAT
}

mfChar MFDF58554B[2*MFSIZE]={0}; // 2

void mfDF58554B(void) { // BUFFER __DBL
  mfpush(MFDF58554B);
}

mfCell MFE37AEDD9=0; // 0

void mfE37AEDD9(void) { // VARIABLE __DPL
  mfpush(&MFE37AEDD9);
}

mfCell MF60DFCE8E=0; // 0

void mf60DFCE8E(void) { // VARIABLE __BASE
  mfpush(&MF60DFCE8E);
}

mfCell MFCC4D88AE=0; // 0

void mfCC4D88AE(void) { // VARIABLE __SIGN
  mfpush(&MFCC4D88AE);
}

void mf15208282(void) { // : __NUMADJ
  mfEF6476DC(); // 2DROP
  mfpush(&MFCC4D88AE); // __SIGN
  mfC50BF85F(); // @
mfIF
  mf8B105859(); // DNEGATE
mfTHEN
  mfE0839FF6(); // 2DUP
  mfpush(&MFDF58554B); // __DBL
  mf9CF2B11C(); // 2!
  mfpush(&MF60DFCE8E); // __BASE
  mfC50BF85F(); // @
  mfpush(&MFE972DB58); // BASE
  mf240C8DEC(); // !
}

void mf867FFF4D(void) { // : NUMBER?
  mf57E80646(); // DUP
  mf13ED5C43(); // 0>
mfNOTIF
  mfEF6476DC(); // 2DROP
  mfpush(0); // $0
mfEXIT
mfTHEN
  mfpush(-1); // $FFFFFFFF
  mfpush(&MFE37AEDD9); // __DPL
  mf240C8DEC(); // !
  mfpush(&MFE972DB58); // BASE
  mfC50BF85F(); // @
  mfpush(&MF60DFCE8E); // __BASE
  mf240C8DEC(); // !
  mf2FE7860F(); // OVER
  mf76DA1336(); // C@
mfCASE
  mfpush(36); // $24
mfOF
  mf2BEE4576(); // CHAR/
  mf818F192A(); // HEX
mfENDOF
  mfpush(35); // $23
mfOF
  mf2BEE4576(); // CHAR/
  mf48AF9A2C(); // DECIMAL
mfENDOF
  mfpush(37); // $25
mfOF
  mf2BEE4576(); // CHAR/
  mfpush(2); // $2
  mfpush(&MFE972DB58); // BASE
  mf240C8DEC(); // !
mfENDOF
  mfpush(46); // $2E
mfOF
  mf57E80646(); // DUP
  mfpush(1); // $1
  mf380CAD68(); // =
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(0); // $0
mfEXIT
mfTHEN
mfENDOF
  mfpush(39); // $27
mfOF
  mf57E80646(); // DUP
  mfpush(3); // $3
  mf380CAD68(); // =
mfIF
  mf2FE7860F(); // OVER
  mfpush(2); // $2
  mf2E0C9DAA(); // +
  mf76DA1336(); // C@
  mfpush(39); // $27
  mf380CAD68(); // =
mfIF
  mf52C16B0C(); // DROP
  mf26EB3B95(); // 1+
  mf76DA1336(); // C@
  mfpush(1); // $1
mfEXIT
mfTHEN
mfTHEN
mfENDOF
mfENDCASE
  mfpush(0); // $0
  mfpush(&MFCC4D88AE); // __SIGN
  mf240C8DEC(); // !
  mf2FE7860F(); // OVER
  mf76DA1336(); // C@
mfCASE
  mfpush(43); // $2B
mfOF
  mf2BEE4576(); // CHAR/
mfENDOF
  mfpush(45); // $2D
mfOF
  mf2BEE4576(); // CHAR/
  mfpush(-1); // $FFFFFFFF
  mfpush(&MFCC4D88AE); // __SIGN
  mf240C8DEC(); // !
mfENDOF
mfENDCASE
  mfpush(0); // $0
  mfpush(0); // $0
  mf404D123E(); // 2SWAP
  mf5773A68C(); // >NUMBER
mfDUPIF
  mf2FE7860F(); // OVER
  mf76DA1336(); // C@
  mfpush(46); // $2E
  mf380CAD68(); // =
mfIF
  mf2BEE4576(); // CHAR/
  mf57E80646(); // DUP
  mfpush(&MFE37AEDD9); // __DPL
  mf240C8DEC(); // !
  mf5773A68C(); // >NUMBER
  mf57E80646(); // DUP
mfNOTIF
  mf15208282(); // __NUMADJ
  mfpush(2); // $2
mfEXIT
mfTHEN
mfTHEN
  mfEF6476DC(); // 2DROP
  mfEF6476DC(); // 2DROP
  mfpush(-1); // $FFFFFFFF
  mfpush(&MFE37AEDD9); // __DPL
  mf240C8DEC(); // !
  mfpush(&MF60DFCE8E); // __BASE
  mfC50BF85F(); // @
  mfpush(&MFE972DB58); // BASE
  mf240C8DEC(); // !
  mfpush(0); // $0
mfEXIT
mfTHEN
  mfpush(2); // $2
  mfE67669F8(); // PICK
  mf67FCEB09(); // >R
  mf15208282(); // __NUMADJ
  mf130456D1(); // R>
mfIF
  mfpush(2); // $2
mfELSE
  mf52C16B0C(); // DROP
  mfpush(1); // $1
mfTHEN
}

static inline void mfD9BC5E8A(void) { // : EMIT
  mferr(mfin(1)); 
  mfemit(mfpop());
}

const mfCell MF20DD5D6B=32; // 32

void mf20DD5D6B(void) { // CONSTANT BL
  mfpush(MF20DD5D6B);
}

void mf1808F0E5(void) { // : SPACE
  mfpush(MF20DD5D6B); // BL
  mfD9BC5E8A(); // EMIT
}

void mf8C12EE82(void) { // : SPACES
mfFOR
  mf1808F0E5(); // SPACE
mfNEXT
}

static inline void mf64D9F6E0(void) { // : CR
  mfemit('\n');
}

void mf142FE78D(void) { // : TYPE
  mferr(mfin(2)); 
  mftype(mfsec,mftos), mf2drop;
}

void mf6B388877(void) { // : _N>DIGIT
  mfpush(&MFE972DB58); // BASE
  mfC50BF85F(); // @
  mfsec=mfn2dig(mfsec,mftos); mftos=(mfsec==256 ? 0 : -1);
}

mfCell MF4DCEA979=0; // 0

void mf4DCEA979(void) { // VARIABLE __HLD
  mfpush(&MF4DCEA979);
}

void mfEBB64F78(void) { // : HOLD
  mfpush(&MF4DCEA979); // __HLD
  mfC50BF85F(); // @
  mf20EB3223(); // 1-
  mf57E80646(); // DUP
  mfpush(&MF4DCEA979); // __HLD
  mf240C8DEC(); // !
  mf97DA4729(); // C!
}

void mf3AFEA0B1(void) { // : HOLDS
mfBEGIN
mfDUPWHILE
  mf20EB3223(); // 1-
  mfE0839FF6(); // 2DUP
  mf2E0C9DAA(); // +
  mf76DA1336(); // C@
  mfEBB64F78(); // HOLD
mfREPEAT
  mfEF6476DC(); // 2DROP
}

void mfA6F73E08(void) { // : <#
  mf9E0B1B0A(); // PAD
  mf20EB3223(); // 1-
  mfpush(0); // $0
  mf2FE7860F(); // OVER
  mf97DA4729(); // C!
  mfpush(&MF4DCEA979); // __HLD
  mf240C8DEC(); // !
}

void mf260C9112(void) { // : #
  mfpush(&MFE972DB58); // BASE
  mfC50BF85F(); // @
  mf1691D15F(); // _MU/MOD
  mf87E8362E(); // ROT
  mf6B388877(); // _N>DIGIT
  mf52C16B0C(); // DROP
  mfEBB64F78(); // HOLD
}

void mf11C88844(void) { // : #>
  mfEF6476DC(); // 2DROP
  mfpush(&MF4DCEA979); // __HLD
  mfC50BF85F(); // @
  mf9E0B1B0A(); // PAD
  mf20EB3223(); // 1-
  mf2FE7860F(); // OVER
  mf280C9438(); // -
}

void mf26C8A953(void) { // : #S
mfBEGIN
  mf260C9112(); // #
  mfE0839FF6(); // 2DUP
  mf7CE4AA04(); // OR
mfNOTUNTIL
}

void mf0AADBFA4(void) { // : SIGN
  mf15ED5F69(); // 0<
mfIF
  mfpush(45); // $2D
  mfEBB64F78(); // HOLD
mfTHEN
}

void mfE4E51507(void) { // : _(.)
  mf57E80646(); // DUP
  mf6C84489B(); // ABS
  mfpush(0); // $0
  mfA6F73E08(); // <#
  mf26C8A953(); // #S
  mf87E8362E(); // ROT
  mf0AADBFA4(); // SIGN
  mf11C88844(); // #>
}

void mf2B0C98F1(void) { // : .
  mfE4E51507(); // _(.)
  mf142FE78D(); // TYPE
  mf1808F0E5(); // SPACE
}

void mf67D44899(void) { // : .R
  mf67FCEB09(); // >R
  mfE4E51507(); // _(.)
  mf130456D1(); // R>
  mf2FE7860F(); // OVER
  mf280C9438(); // -
  mf8C12EE82(); // SPACES
  mf142FE78D(); // TYPE
}

void mfFD90D4E8(void) { // : _(U.)
  mfpush(0); // $0
  mfA6F73E08(); // <#
  mf26C8A953(); // #S
  mf11C88844(); // #>
}

void mf20F323BA(void) { // : U.
  mfFD90D4E8(); // _(U.)
  mf142FE78D(); // TYPE
  mf1808F0E5(); // SPACE
}

void mfC6C18638(void) { // : U.R
  mf67FCEB09(); // >R
  mfFD90D4E8(); // _(U.)
  mf130456D1(); // R>
  mf2FE7860F(); // OVER
  mf280C9438(); // -
  mf8C12EE82(); // SPACES
  mf142FE78D(); // TYPE
}

void mfACCAE4CC(void) { // : KEY
  mferr(mfout(1)); 
  mfpush(mfkey());
}

void mf209E17E9(void) { // : ACCEPT
  mfpush(0); // $0
mfBEGIN
  mfACCAE4CC(); // KEY
  mf57E80646(); // DUP
  mfpush(13); // $D
  mf93F7201F(); // <>
mfWHILE
  mf57E80646(); // DUP
  mfpush(3); // $3
  mf380CAD68(); // =
  mf2FE7860F(); // OVER
  mfpush(27); // $1B
  mf380CAD68(); // =
  mf7CE4AA04(); // OR
mfIF
  mf52C16B0C(); // DROP
  mfpush(0); // $0
mfBREAK
mfTHEN
  mf57E80646(); // DUP
  mfpush(8); // $8
  mf380CAD68(); // =
mfIF
  mf52C16B0C(); // DROP
mfDUPIF
  mfpush(8); // $8
  mfD9BC5E8A(); // EMIT
  mf1808F0E5(); // SPACE
  mfpush(8); // $8
  mfD9BC5E8A(); // EMIT
  mf20EB3223(); // 1-
mfTHEN
mfCONTINUE
mfTHEN
  mf67FCEB09(); // >R
  mfE0839FF6(); // 2DUP
  mf3B0CB221(); // >
  mf130456D1(); // R>
  mfA2DF200E(); // SWAP
mfIF
  mf57E80646(); // DUP
  mfD9BC5E8A(); // EMIT
  mf2FE7860F(); // OVER
  mfpush(4); // $4
  mfE67669F8(); // PICK
  mf2E0C9DAA(); // +
  mf97DA4729(); // C!
  mf26EB3B95(); // 1+
mfELSE
  mf52C16B0C(); // DROP
mfTHEN
mfREPEAT
  mf52C16B0C(); // DROP
  mf67FCEB09(); // >R
  mfEF6476DC(); // 2DROP
  mf130456D1(); // R>
}

void mf0119CBC8(void) { // : _#IB
  mferr(mfout(1)); 
  mfpush(MFIB);
}

void mfC49B66C5(void) { // : _TIB
  mferr(mfout(1)); 
  static mfChar tib[MFIB]; mfpush(tib);
}

void mfB3D82B11(void) { // : _SIBS
  mferr(mfout(1)); 
  static mfChar sibs[4*(MFIB+2)]; mfpush(sibs);
}

mfCell MF0FF74456=0; // 0

void mf0FF74456(void) { // VARIABLE __SIBID
  mfpush(&MF0FF74456);
}

void mf12AE9718(void) { // : _SIB
  mfB3D82B11(); // _SIBS
  mfpush(&MF0FF74456); // __SIBID
  mfC50BF85F(); // @
  mf0119CBC8(); // _#IB
  mfpush(2); // $2
  mf2E0C9DAA(); // +
  mf2F0C9F3D(); // *
  mf2E0C9DAA(); // +
}

void mfCFEC5BB6(void) { // : _>SIB
  mf0119CBC8(); // _#IB
  mfpush(2); // $2
  mf280C9438(); // -
  mf0E45F4B7(); // MIN
  mfpush(&MF0FF74456); // __SIBID
  mfC50BF85F(); // @
  mf26EB3B95(); // 1+
  mfpush(3); // $3
  mf91666DC6(); // AND
  mfpush(&MF0FF74456); // __SIBID
  mf240C8DEC(); // !
  mf12AE9718(); // _SIB
  mf10E38971(); // _PLACE
}

mfChar MF01A69217[5*MFSIZE]={0}; // 5

void mf01A69217(void) { // BUFFER _SRCSPEC
  mfpush(MF01A69217);
}

void mf7C1EEF6B(void) { // : _SRCLEN
  mfpush(&MF01A69217); // _SRCSPEC
}

void mf7A91D589(void) { // : _SRCADR
  mfpush(&MF01A69217); // _SRCSPEC
  mfBB8FCA7A(); // CELL+
}

void mfFD898658(void) { // : _SRCID!
  mfpush(&MF01A69217); // _SRCSPEC
  mfpush(4); // $4
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
  mf240C8DEC(); // !
}

void mf5AF98C79(void) { // : _SRC2!
  mfpush(&MF01A69217); // _SRCSPEC
  mf9CF2B11C(); // 2!
}

void mf600421B4(void) { // : _SRC>R
  mfpush(&MF01A69217); // _SRCSPEC
  mfpush(5); // $5
  mfpush(1); // $1
  mf838F7252(); // CELLS
mfFORUP
mfN
  mfC50BF85F(); // @
  mf67FCEB09(); // >R
mfNEXT
}

void mfD24E2A14(void) { // : _R>SRC
  mfpush(&MF01A69217); // _SRCSPEC
  mfpush(5); // $5
  mfpush(1); // $1
  mf838F7252(); // CELLS
mfDOWNFOR
  mf130456D1(); // R>
mfN
  mf240C8DEC(); // !
mfNEXT
}

void mfB9790298(void) { // : SOURCE
  mfpush(&MF01A69217); // _SRCSPEC
  mf3DF21B8F(); // 2@
}

void mf580B1E42(void) { // : >IN
  mfpush(&MF01A69217); // _SRCSPEC
  mfpush(2); // $2
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
}

void mf254113A2(void) { // : SOURCE-ID
  mfpush(&MF01A69217); // _SRCSPEC
  mfpush(4); // $4
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
  mfC50BF85F(); // @
}

void mfEF18D029(void) { // : _BLK
  mfpush(&MF01A69217); // _SRCSPEC
  mfpush(3); // $3
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
}

void mf94A8FFED(void) { // : SAVE-INPUT
  mfpush(&MF01A69217); // _SRCSPEC
  mfpush(5); // $5
  mfpush(1); // $1
  mf838F7252(); // CELLS
mfFORUP
mfN
  mfC50BF85F(); // @
mfNEXT
  mfpush(5); // $5
}

void mfA10A7AB8(void) { // : RESTORE-INPUT
  mfpush(5); // $5
  mf93F7201F(); // <>
  mf2FE7860F(); // OVER
  mf254113A2(); // SOURCE-ID
  mf93F7201F(); // <>
  mf7CE4AA04(); // OR
mfIF
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfpush(&MF01A69217); // _SRCSPEC
  mfpush(5); // $5
  mfpush(1); // $1
  mf838F7252(); // CELLS
mfDOWNFOR
mfN
  mf240C8DEC(); // !
mfNEXT
  mfpush(MFEE597878); // FALSE
}

void mfC1D4E711(void) { // : REFILL
  mf254113A2(); // SOURCE-ID
mfNOTIF
  mfC49B66C5(); // _TIB
  mf57E80646(); // DUP
  mf0119CBC8(); // _#IB
  mf209E17E9(); // ACCEPT
  mf5AF98C79(); // _SRC2!
  mfpush(0); // $0
  mf580B1E42(); // >IN
  mf240C8DEC(); // !
  mfpush(MF501B0925); // TRUE
mfELSE
  mfpush(MFEE597878); // FALSE
mfTHEN
}

void mfD66336C5(void) { // : _SKIP
  mferr(mfin(3)); 
  mfChar c=(mfChar)mfpop(), u=(mfChar)mftos, *a=(mfChar*)mfsec; 
  while(u>0) { 
    if (c==32) { if (*a>32) break; } else { if (*a!=c) break; }    
    a++,u--; } 
  mfsec=(mfCell)a, mftos=u;
}

void mfC119E57B(void) { // : _SCAN
  mferr(mfin(3)); 
  mfChar c=(mfChar)mfpop(), u=(mfChar)mftos, *a=(mfChar*)mfsec; 
  while(u>0) { 
    if (c==32) { if (*a<=32) break; } else { if (*a==c) break; }    
    a++,u--; } 
  mfsec=(mfCell)a, mftos=u;
}

void mfB8B7D9B3(void) { // : __PRSAREA
  mfB9790298(); // SOURCE
  mf580B1E42(); // >IN
  mfC50BF85F(); // @
  mf62A404AF(); // /STRING
  mfpush(0); // $0
  mf1C599279(); // MAX
}

void mf60C7C223(void) { // : __PRSOVF
  mf7C1EEF6B(); // _SRCLEN
  mfC50BF85F(); // @
  mf580B1E42(); // >IN
  mfC50BF85F(); // @
  mf390CAEFB(); // <
mfIF
  mfpush(-18); // $FFFFFFEE
  (MFE5B4B40F)(); // THROW
mfTHEN
}

void mfADFC5551(void) { // : _PARSE
  mf2FE7860F(); // OVER
  mf67FCEB09(); // >R
  mf87E8362E(); // ROT
  mfC119E57B(); // _SCAN
  mf20EB3223(); // 1-
  mf7C1EEF6B(); // _SRCLEN
  mfC50BF85F(); // @
  mfA2DF200E(); // SWAP
  mf280C9438(); // -
  mf580B1E42(); // >IN
  mf240C8DEC(); // !
  mf130456D1(); // R>
  mf93A82DE8(); // TUCK
  mf280C9438(); // -
}

void mfD918C10C(void) { // : PARSE
  mfB8B7D9B3(); // __PRSAREA
  mfADFC5551(); // _PARSE
}

void mfF08832C0(void) { // : _PARSE-CHAR
  mfD918C10C(); // PARSE
  mf60C7C223(); // __PRSOVF
}

void mfE9BCE12F(void) { // : _PARSE-NAME
  mfB8B7D9B3(); // __PRSAREA
  mfpush(2); // $2
  mfE67669F8(); // PICK
  mfD66336C5(); // _SKIP
  mfADFC5551(); // _PARSE
}

void mfF92179F8(void) { // : PARSE-NAME
  mfpush(MF20DD5D6B); // BL
  mfE9BCE12F(); // _PARSE-NAME
}

mfChar MFF939F00F[2*MFSIZE]={0}; // 2

void mfF939F00F(void) { // BUFFER _PARSED
  mfpush(MFF939F00F);
}

mfCell MFA274DA18=-1; // -1

void mfA274DA18(void) { // VARIABLE CAPS
  mfpush(&MFA274DA18);
}

void mf6D01E13D(void) { // : WORD
  mfE9BCE12F(); // _PARSE-NAME
  mfE0839FF6(); // 2DUP
  mfpush(&MFF939F00F); // _PARSED
  mf9CF2B11C(); // 2!
  mf5F2B6B8B(); // HERE
  mf10E38971(); // _PLACE
  mfpush(&MFA274DA18); // CAPS
  mfC50BF85F(); // @
mfIF
  mf5F2B6B8B(); // HERE
  mfA3F7B2D4(); // COUNT
  mfEE2653C2(); // _UPPER
mfTHEN
  mf5F2B6B8B(); // HERE
}

void mfC94E167C(void) { // : _PARSE-WORD
  mfpush(MF20DD5D6B); // BL
  mf6D01E13D(); // WORD
  mfA3F7B2D4(); // COUNT
}

void mfA59F665D(void) { // : CHAR
  mfF92179F8(); // PARSE-NAME
  mf52C16B0C(); // DROP
  mf76DA1336(); // C@
}

void mfD90C17DB(void) { // : |
  mf7C1EEF6B(); // _SRCLEN
  mfC50BF85F(); // @
  mf580B1E42(); // >IN
  mf240C8DEC(); // !
} // IMMEDIATE 

void mf2D0C9C17(void) { // : (
  mfpush(41); // $29
  mfF08832C0(); // _PARSE-CHAR
  mfEF6476DC(); // 2DROP
} // IMMEDIATE 

void mf9DD49D9B(void) { // : .(
  mfpush(41); // $29
  mfF08832C0(); // _PARSE-CHAR
  mf142FE78D(); // TYPE
} // IMMEDIATE 

void mf0E9A8AA9(void) { // : _FIND
  mferr(mfin(3)); 
  mfHdr *p,*h=(mfHdr*)mfat(mfpop()); char *n,u=mfpop(),*a=(char*)mftos; 
  mftos=0; 
  while (p=h->link,p) { n=h->nfa; 
    if ((u==*n)&&(*a==*++n)&&!strncmp(n,a,u)) {  
    mfUCell f=h->pfa; f=(f&1?1:-1)*(f&2?2:1); 
      mftos=(mfCell)&h->cfa, mfpush(f); 
      return; } 
    h=p; }
}

void mf85A4C44F(void) { // : _WORDLISTS
  mferr(mfout(1)); 
  mfpush(MFFWL);
}

void mf82CE5579(void) { // : _CONTEXT
  mferr(mfout(1)); 
  mfpush(MFCTX);
}

void mf5AADB2E4(void) { // : _FIND-WORD
  mf82CE5579(); // _CONTEXT
  mfpush(8); // $8
  mfpush(1); // $1
  mf838F7252(); // CELLS
mfFORUP
mfN
  mfC50BF85F(); // @
mfNOTIF
mfBREAK
mfTHEN
  mfE0839FF6(); // 2DUP
mfN
  mfC50BF85F(); // @
  mf0E9A8AA9(); // _FIND
  mf8003A4B3(); // ?DUP
mfIF
  mf404D123E(); // 2SWAP
  mfEF6476DC(); // 2DROP
mfEXIT
mfTHEN
mfNEXT
  mfEF6476DC(); // 2DROP
  mfpush(0); // $0
}

void mfFB42BA5A(void) { // : FIND
  mf57E80646(); // DUP
  mfA3F7B2D4(); // COUNT
  mf5AADB2E4(); // _FIND-WORD
mfDUPIF
  mf87E8362E(); // ROT
  mf52C16B0C(); // DROP
mfTHEN
}

void mf220C8AC6(void) { // : '
  mfC94E167C(); // _PARSE-WORD
  mf5AADB2E4(); // _FIND-WORD
mfNOTIF
  mfpush(-13); // $FFFFFFF3
  (MFE5B4B40F)(); // THROW
mfTHEN
}

void mfCF77DFB8(void) { // : EXECUTE
  mferr(mfin(1)); 
  mfw=(mfXT*)mfpop(); (*mfw)();
}

mfCell MFE09A6F96=0; // 0

void mfE09A6F96(void) { // VARIABLE STATE
  mfpush(&MFE09A6F96);
}

void mf290C95CB(void) { // : ,
  mferr(mfin(1)); 
  mfat(mfdp)=mfpop(), mfdp+=MFSIZE;
}

void mf1479C7E3(void) { // : _[LIT]
  mferr(mfout(1)); 
  mfpush(*mfip++);
}

void mfBAFC44C4(void) { // : LITERAL
  mfat(mfdp)=(mfCell)&mfdict[173].cfa;
  mfdp+=MFSIZE; // [,] _[LIT]
  mf290C95CB(); // ,
} // IMMEDIATE COMPILE-ONLY

mfXT MF26F8E50E=mf64EE01EC; // IS ___LITERAL?

void mf26F8E50E(void) { // DEFER _LITERAL?
  (MF26F8E50E)();
}

void mf64EE01EC(void) { // : ___LITERAL?
  mfpush(MFEE597878); // FALSE
}

void mf87601DFB(void) { // : _INTERPRET
mfBEGIN
  mfC94E167C(); // _PARSE-WORD
  mf57E80646(); // DUP
mfWHILE
  mf5AADB2E4(); // _FIND-WORD
  mf8003A4B3(); // ?DUP
mfIF
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
mfIF
  mf15ED5F69(); // 0<
mfIF
  mf290C95CB(); // ,
mfELSE
  mfCF77DFB8(); // EXECUTE
mfTHEN
mfELSE
  mfpush(1); // $1
  mf91666DC6(); // AND
mfIF
  mfCF77DFB8(); // EXECUTE
mfELSE
  mfpush(-14); // $FFFFFFF2
  (MFE5B4B40F)(); // THROW
mfTHEN
mfTHEN
mfELSE
  mfpush(&MFF939F00F); // _PARSED
  mf3DF21B8F(); // 2@
  mf867FFF4D(); // NUMBER?
  mf8003A4B3(); // ?DUP
mfNOTIF
  (MF26F8E50E)(); // _LITERAL?
mfNOTIF
  mfpush(-13); // $FFFFFFF3
  (MFE5B4B40F)(); // THROW
mfTHEN
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
mfIF
  mf63DECBC8(); // NIP
mfELSE
  mf52C16B0C(); // DROP
mfTHEN
  mfCF77DFB8(); // EXECUTE
mfELSE
  mfpush(2); // $2
  mf380CAD68(); // =
mfIF
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
mfIF
  mfA2DF200E(); // SWAP
  mfBAFC44C4(); // LITERAL
mfTHEN
mfTHEN
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
mfIF
  mfBAFC44C4(); // LITERAL
mfTHEN
mfTHEN
mfTHEN
mfREPEAT
  mfEF6476DC(); // 2DROP
}

void mf9FC8664E(void) { // : _EXCSRC
mfDUPIF
  mf64D9F6E0(); // CR
  mfprint("? "); // S"
  mfB9790298(); // SOURCE
  mfpush(1); // $1
mfFORUP
mfN
  mf76DA1336(); // C@
  mfpush(MF20DD5D6B); // BL
  mf1C599279(); // MAX
  mfD9BC5E8A(); // EMIT
mfNEXT
  mf64D9F6E0(); // CR
  mf580B1E42(); // >IN
  mfC50BF85F(); // @
  mf8C12EE82(); // SPACES
  mfprint("^ ?? "); // S"
mfTHEN
}

void mf93D1E229(void) { // : _EXCMSG
mfCASE
  mfpush(-3); // $FFFFFFFD
mfOF
  mfprint("stack overflow"); // S"
mfENDOF
  mfpush(-4); // $FFFFFFFC
mfOF
  mfprint("stack underflow"); // S"
mfENDOF
  mfpush(-5); // $FFFFFFFB
mfOF
  mfprint("return stack overflow"); // S"
mfENDOF
  mfpush(-6); // $FFFFFFFA
mfOF
  mfprint("return stack underflow"); // S"
mfENDOF
  mfpush(-9); // $FFFFFFF7
mfOF
  mfprint("invalid memory address"); // S"
mfENDOF
  mfpush(-10); // $FFFFFFF6
mfOF
  mfprint("division by zero"); // S"
mfENDOF
  mfpush(-13); // $FFFFFFF3
mfOF
  mfprint("undefined word"); // S"
mfENDOF
  mfpush(-14); // $FFFFFFF2
mfOF
  mfprint("interpreting a compile-only word"); // S"
mfENDOF
  mfpush(-18); // $FFFFFFEE
mfOF
  mfprint("parsed string overflow"); // S"
mfENDOF
  mfpush(-22); // $FFFFFFEA
mfOF
  mfprint("control-structure mismatch"); // S"
mfENDOF
  mfpush(-24); // $FFFFFFE8
mfOF
  mfprint("invalid numeric argument"); // S"
mfENDOF
  mfpush(-32); // $FFFFFFE0
mfOF
  mfprint("invalid name argument"); // S"
mfENDOF
  mfpush(-37); // $FFFFFFDB
mfOF
  mfprint("file I/O exception"); // S"
mfENDOF
  mfpush(-38); // $FFFFFFDA
mfOF
  mfprint("non-existent or locked file"); // S"
mfENDOF
  mf57E80646(); // DUP
  mfprint("exception "); // S"
  mf2B0C98F1(); // .
mfENDCASE
}

void mfEAF28AAC(void) { // : CATCH
  int xc; mfXcf xcf; // local exception frame 
  xcf.sp=mfsp-1, xcf.lp=mflp, xcf.fp=mffp, xcf.rp=mfrp, xcf.ip=mfip; 
  xcf.old=mfxcf, mfxcf=&xcf;    
  if (xc=setjmp(xcf.buf),xc) { // unwind 
     mfsp=xcf.sp, mflp=xcf.lp, mffp=xcf.fp, mfrp=xcf.rp, mfip=xcf.ip; } 
  else { // execute 
     mfw=(mfXT*)mfpop(); (*mfw)(); }  
  mfxcf=xcf.old, mfpush(xc);
}

void mf773B319E(void) { // : _THROW
mfDUPIF
    if (mfxcf) longjmp(mfxcf->buf,mftos); // unwind stacks 
  mf57E80646(); // DUP
  mfpush(-1); // $FFFFFFFF
  mf380CAD68(); // =
mfIF
  mf52C16B0C(); // DROP
mfELSE
  mf57E80646(); // DUP
  mfpush(-2); // $FFFFFFFE
  mf380CAD68(); // =
mfIF
  mf52C16B0C(); // DROP
  mf142FE78D(); // TYPE
mfELSE
  mf9FC8664E(); // _EXCSRC
  mf93D1E229(); // _EXCMSG
mfTHEN
mfTHEN
    longjmp(mfabort,-1); // reset stacks 
mfTHEN
  mf52C16B0C(); // DROP
}

void mf319DE64A(void) { // : EVALUATE
  mf600421B4(); // _SRC>R
  mf5AF98C79(); // _SRC2!
  mfpush(0); // $0
  mf580B1E42(); // >IN
  mf240C8DEC(); // !
  mfpush(-1); // $FFFFFFFF
  mfFD898658(); // _SRCID!
  mfpush(0); // $0
  mfEF18D029(); // _BLK
  mf240C8DEC(); // !
  mfpush(&mfdict[176].cfa); // ['] _INTERPRET
  mfEAF28AAC(); // CATCH
mfDUPIF
  mf9FC8664E(); // _EXCSRC
  mfprint("in string"); // S"
mfTHEN
  mfD24E2A14(); // _R>SRC
  (MFE5B4B40F)(); // THROW
}

void mfE0018F77(void) { // : _STACKS
  mfCE61558A(); // DEPTH
mfFOR
  mfCE61558A(); // DEPTH
  mf20EB3223(); // 1-
mfN
  mf280C9438(); // -
  mfE67669F8(); // PICK
  mf2B0C98F1(); // .
mfNEXT
}

void mf2C72BE34(void) { // : _PROMPT
  mfE0018F77(); // _STACKS
  mfpush(&MFE972DB58); // BASE
  mfC50BF85F(); // @
  mf57E80646(); // DUP
  mfpush(10); // $A
  mf380CAD68(); // =
mfIF
  mf52C16B0C(); // DROP
  mfpush(35); // $23
mfELSE
  mfpush(16); // $10
  mf380CAD68(); // =
mfIF
  mfpush(36); // $24
mfELSE
  mfpush(126); // $7E
mfTHEN
mfTHEN
  mfD9BC5E8A(); // EMIT
  mf1808F0E5(); // SPACE
}

void mf84D9BC36(void) { // : QUIT
  mfpush(0); // $0
  mf8B51E6F7(); // _RP!
  mfpush(0); // $0
  mfFD898658(); // _SRCID!
  mfpush(0); // $0
  mfpush(&MFE09A6F96); // STATE
  mf240C8DEC(); // !
mfBEGIN
  mf64D9F6E0(); // CR
  mf2C72BE34(); // _PROMPT
  mfC1D4E711(); // REFILL
  mf1808F0E5(); // SPACE
mfWHILE
  mfpush(&mfdict[176].cfa); // ['] _INTERPRET
  mfEAF28AAC(); // CATCH
  mf8003A4B3(); // ?DUP
mfIF
  (MFE5B4B40F)(); // THROW
mfELSE
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
mfNOTIF
  mfprint(" ok"); // S"
mfTHEN
mfTHEN
mfREPEAT
}

void mf28B3832A(void) { // : _BOOT
  mfinit();
}

void mf49997C51(void) { // : _LOGO
  mfprint("MinForth V3.4e"); // S"
}

void mf946BA378(void) { // : _ABORT
  setjmp(mfabort); 
  mfpush(0); // $0
  mf89C518BC(); // _SP!
  mf84D9BC36(); // QUIT
}

void mf367DE7D9(void) { // : ABORT
  mfpush(-1); // $FFFFFFFF
  (MFE5B4B40F)(); // THROW
}

void mfB6A8BBC3(void) { // : BYE
  mf64D9F6E0(); // CR
  mfprint("Bye. "); // S"
  mfexit(), exit(0);
}

void mf8A823C60(void) { // : _>LINK
}

void mf4B2037E9(void) { // : _>PFA
  mfBB8FCA7A(); // CELL+
}

void mf03D7EB73(void) { // : _>NAME
  mfpush(2); // $2
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
  mfC50BF85F(); // @
}

void mf74404CD4(void) { // : _>CFA
  mfpush(3); // $3
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
}

void mfC439E65E(void) { // : _<CFA
  mfpush(3); // $3
  mf838F7252(); // CELLS
  mf280C9438(); // -
}

void mfE345B847(void) { // : _CURRENT
  mferr(mfout(1)); 
  static void* cur=MFFWL; mfpush(&cur);
}

void mfB8F49A42(void) { // : _LAST
  mferr(mfout(1)); 
  mfpush(&MFLAST);
}

void mfEA08BD65(void) { // : _LATEST
  mferr(mfout(1)); 
  mfpush(&MFLATEST);
}

mfCell MFEF0255CF=-1; // -1

void mfEF0255CF(void) { // VARIABLE WARNING
  mfpush(&MFEF0255CF);
}

void mf20B9677F(void) { // : _HEADER
  mf57E80646(); // DUP
mfNOTIF
  mfpush(-32); // $FFFFFFE0
  (MFE5B4B40F)(); // THROW
mfTHEN
  mf5AADB2E4(); // _FIND-WORD
mfIF
  mf52C16B0C(); // DROP
  mfpush(&MFEF0255CF); // WARNING
  mfC50BF85F(); // @
mfIF
  mfprint(" ? redefined "); // S"
  mf5F2B6B8B(); // HERE
  mfA3F7B2D4(); // COUNT
  mf142FE78D(); // TYPE
  mf1808F0E5(); // SPACE
mfTHEN
mfTHEN
  mf5F2B6B8B(); // HERE
  mf57E80646(); // DUP
  mf76DA1336(); // C@
  mfpush(2); // $2
  mf2E0C9DAA(); // +
  mf3FA1943F(); // ALLOT
  mfAFA183BE(); // ALIGN
  mfEA08BD65(); // _LATEST
  mfC50BF85F(); // @
  mf290C95CB(); // ,
  mf5F2B6B8B(); // HERE
  mfEA08BD65(); // _LATEST
  mf240C8DEC(); // !
  mfE345B847(); // _CURRENT
  mfC50BF85F(); // @
  mfC50BF85F(); // @
  mf290C95CB(); // ,
  mfpush(0); // $0
  mf290C95CB(); // ,
  mf290C95CB(); // ,
  mfC50BF85F(); // @
  mf290C95CB(); // ,
}

void mf3BFC9593(void) { // : _REVEAL
  mfEA08BD65(); // _LATEST
  mfC50BF85F(); // @
  mf03D7EB73(); // _>NAME
  mfC50BF85F(); // @
mfIF
  mfEA08BD65(); // _LATEST
  mfC50BF85F(); // @
  mf57E80646(); // DUP
  mfE345B847(); // _CURRENT
  mfC50BF85F(); // @
  mf240C8DEC(); // !
  mfB8F49A42(); // _LAST
  mf240C8DEC(); // !
mfTHEN
}

void mf779E612E(void) { // : _HIDE
  mfB8F49A42(); // _LAST
  mfC50BF85F(); // @
  mf03D7EB73(); // _>NAME
  mfC50BF85F(); // @
mfIF
  mfB8F49A42(); // _LAST
  mfC50BF85F(); // @
  mfC50BF85F(); // @
  mf57E80646(); // DUP
  mfE345B847(); // _CURRENT
  mfC50BF85F(); // @
  mf240C8DEC(); // !
  mfB8F49A42(); // _LAST
  mf240C8DEC(); // !
mfTHEN
}

void mfA483CD31(void) { // : __FLG!
  mfEA08BD65(); // _LATEST
  mfC50BF85F(); // @
  mf4B2037E9(); // _>PFA
  mf93A82DE8(); // TUCK
  mfC50BF85F(); // @
  mf7CE4AA04(); // OR
  mfA2DF200E(); // SWAP
  mf240C8DEC(); // !
}

void mfD1E3DADA(void) { // : IMMEDIATE
  mfpush(1); // $1
  mfA483CD31(); // __FLG!
}

void mf72FB7883(void) { // : COMPILE-ONLY
  mfpush(2); // $2
  mfA483CD31(); // __FLG!
}

void mfDE0C1FBA(void) { // : [
  mfpush(0); // $0
  mfpush(&MFE09A6F96); // STATE
  mf240C8DEC(); // !
} // IMMEDIATE 

void mfD80C1648(void) { // : ]
  mfpush(-1); // $FFFFFFFF
  mfpush(&MFE09A6F96); // STATE
  mf240C8DEC(); // !
}

void mfB988A2D6(void) { // : _[:]
  mferr(mfrout(1)); 
  mfrpush(mfip); mfip=(mfXT**)mfw+1; 
  while(mfw=*mfip++,mfw) (*mfw)(); // MinForth VM 
  mfip=(mfXT**)mfrpop();
}

void mf04316999(void) { // : __CSYS
  mfCE61558A(); // DEPTH
  mfD745D921(); // INVERT
}

void mf3FC95052(void) { // : __CSYS?
  mfCE61558A(); // DEPTH
  mf2E0C9DAA(); // +
mfIF
  mfpush(-22); // $FFFFFFEA
  (MFE5B4B40F)(); // THROW
mfTHEN
}

void mf3F0CB86D(void) { // : :
  mfpush(&mfdict[206].cfa); // ['] _[:]
  mfC94E167C(); // _PARSE-WORD
  mf20B9677F(); // _HEADER
  mf04316999(); // __CSYS
  mfD80C1648(); // ]
}

void mf3E0CB6DA(void) { // : ;
  mfpush(0); // $0
  mf290C95CB(); // ,
  mfDE0C1FBA(); // [
  mf3FC95052(); // __CSYS?
  mf3BFC9593(); // _REVEAL
} // IMMEDIATE COMPILE-ONLY

void mfF0A95069(void) { // : :NONAME
  mf5F2B6B8B(); // HERE
  mfpush(0); // $0
  mf290C95CB(); // ,
  mf5F2B6B8B(); // HERE
  mfEA08BD65(); // _LATEST
  mf240C8DEC(); // !
  mfpush(0); // $0
  mf290C95CB(); // ,
  mfpush(0); // $0
  mf290C95CB(); // ,
  mf290C95CB(); // ,
  mf5F2B6B8B(); // HERE
  mfpush(&mfdict[206].cfa); // ['] _[:]
  mfC50BF85F(); // @
  mf290C95CB(); // ,
  mf04316999(); // __CSYS
  mfD80C1648(); // ]
}

void mfBA444C0E(void) { // : [']
  mf220C8AC6(); // '
  mfBAFC44C4(); // LITERAL
} // IMMEDIATE COMPILE-ONLY

void mfD89985D6(void) { // : COMPILE,
  mf290C95CB(); // ,
}

void mfA66C8E4A(void) { // : [COMPILE]
  mf220C8AC6(); // '
  mfD89985D6(); // COMPILE,
} // IMMEDIATE COMPILE-ONLY

void mf33530D01(void) { // : _POST[
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
  mf67FCEB09(); // >R
  mfD80C1648(); // ]
}

void mf9A7AC789(void) { // : _]PONE
  mf130456D1(); // R>
  mfpush(&MFE09A6F96); // STATE
  mf240C8DEC(); // !
}

void mfDD00FE9F(void) { // : POSTPONE
  mfC94E167C(); // _PARSE-WORD
  mf5AADB2E4(); // _FIND-WORD
  mf57E80646(); // DUP
mfNOTIF
  mfpush(-13); // $FFFFFFF3
  (MFE5B4B40F)(); // THROW
mfTHEN
  mf15ED5F69(); // 0<
mfIF
  mfat(mfdp)=(mfCell)&mfdict[213].cfa;
  mfdp+=MFSIZE; // [,] _POST[
  mfBAFC44C4(); // LITERAL
  mfat(mfdp)=(mfCell)&mfdict[172].cfa;
  mfdp+=MFSIZE; // [,] ,
  mfat(mfdp)=(mfCell)&mfdict[214].cfa;
  mfdp+=MFSIZE; // [,] _]PONE
mfELSE
  mf290C95CB(); // ,
mfTHEN
} // IMMEDIATE COMPILE-ONLY

void mfFDFF8019(void) { // : [CHAR]
  mfA59F665D(); // CHAR
  mfBAFC44C4(); // LITERAL
} // IMMEDIATE COMPILE-ONLY

void mfA2DA587A(void) { // : C,
  mf5F2B6B8B(); // HERE
  mf97DA4729(); // C!
  mfpush(1); // $1
  mf3FA1943F(); // ALLOT
}

void mf7BDCDF68(void) { // : _[SLIT]
  mferr(mfout(2)); 
  mfCell ip=(mfCell)mfip; mfChar c=mfcat(ip); 
  mfpush(ip+1), mfpush(c); 
  mfip=(mfXT**)((ip+c+1+MFSIZE)&-MFSIZE);
}

void mf50BE3701(void) { // : SLITERAL
  mfat(mfdp)=(mfCell)&mfdict[218].cfa;
  mfdp+=MFSIZE; // [,] _[SLIT]
  mfA2DF200E(); // SWAP
  mf2FE7860F(); // OVER
  mf5F2B6B8B(); // HERE
  mf10E38971(); // _PLACE
  mfpush(2); // $2
  mf2E0C9DAA(); // +
  mf901B2AFF(); // ALIGNED
  mf3FA1943F(); // ALLOT
} // IMMEDIATE COMPILE-ONLY

void mfF501E900(void) { // : S"
  mfpush(34); // $22
  mfF08832C0(); // _PARSE-CHAR
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
mfIF
  mf50BE3701(); // SLITERAL
mfELSE
  mfCFEC5BB6(); // _>SIB
  mf12AE9718(); // _SIB
  mfA3F7B2D4(); // COUNT
mfTHEN
} // IMMEDIATE 

void mf94DA4270(void) { // : C"
  mfpush(34); // $22
  mfF08832C0(); // _PARSE-CHAR
  mf50BE3701(); // SLITERAL
  mfat(mfdp)=(mfCell)&mfdict[8].cfa;
  mfdp+=MFSIZE; // [,] DROP
  mfat(mfdp)=(mfCell)&mfdict[49].cfa;
  mfdp+=MFSIZE; // [,] 1-
} // IMMEDIATE COMPILE-ONLY

void mf97D49429(void) { // : ."
  mfpush(34); // $22
  mfF08832C0(); // _PARSE-CHAR
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
mfIF
  mf50BE3701(); // SLITERAL
  mfat(mfdp)=(mfCell)&mfdict[113].cfa;
  mfdp+=MFSIZE; // [,] TYPE
mfELSE
  mf142FE78D(); // TYPE
mfTHEN
} // IMMEDIATE 

void mfAC2233D4(void) { // : __PARSE|"
  mfB8B7D9B3(); // __PRSAREA
  mfCell i, u=mftos, ub=0, l, h; mfChar *a=(mfChar*)mfsec, c;  
  for (i=0;i<u;i++) { c=a[i]; 
    if (c=='\"') break; 
    if (c=='\\') { c=a[++i];  
#ifdef _WIN32 
      if (c=='n') c='m'; 
#endif 
      switch (c) { 
      case '\"': c=34; break; 
      case '\\': c=92; break; 
      case 'r': c=13; break; 
      case 'm': mfbuf[ub++]=13; // fall thru 
    case 'n': // fall thru 
      case 'l': c=10; break; 
      case 'a': c=7; break; 
      case 'b': c=8; break; 
      case 'e': c=27; break; 
      case 'f': c=12; break; 
      case 'q': c=34; break; 
      case 't': c=9; break; 
      case 'v': c=11; break; 
      case 'z': c=0; break; 
      case 'x': h=mfdig2n(a[i+1],16); if (h<16) { 
           l=mfdig2n(a[i+2],16); if (l<16) { 
           i+=2, c=16*h+l; break; } } // fall thru 
      default: mfbuf[ub++]='\\'; } }  
    mfbuf[ub++]=c; } 
  mfsec=(mfCell)mfbuf, mftos=ub, mfpush(i+1);  
  mf580B1E42(); // >IN
  mf08DC01D1(); // +!
  mf60C7C223(); // __PRSOVF
}

void mf853A39E8(void) { // : S|"
  mfAC2233D4(); // __PARSE|"
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
mfIF
  mf50BE3701(); // SLITERAL
mfELSE
  mfCFEC5BB6(); // _>SIB
  mf12AE9718(); // _SIB
  mfA3F7B2D4(); // COUNT
mfTHEN
} // IMMEDIATE 

void mfFF7226AE(void) { // : _ABORT"
  mf87E8362E(); // ROT
mfIF
  mfpush(-2); // $FFFFFFFE
  (MFE5B4B40F)(); // THROW
mfELSE
  mfEF6476DC(); // 2DROP
mfTHEN
}

void mfC3343021(void) { // : ABORT"
  mfpush(34); // $22
  mfF08832C0(); // _PARSE-CHAR
  mf50BE3701(); // SLITERAL
  mfat(mfdp)=(mfCell)&mfdict[224].cfa;
  mfdp+=MFSIZE; // [,] _ABORT"
} // IMMEDIATE COMPILE-ONLY

void mf79836105(void) { // : EXIT
  mfpush(0); // $0
  mf290C95CB(); // ,
} // IMMEDIATE COMPILE-ONLY

void mf992CB91D(void) { // : _[JMP]
  mfip+=mfat(mfip);
}

void mf6156569F(void) { // : _[JMPZ]
  mfip+=(mfpop()?1:mfat(mfip));
}

void mf9592F3FF(void) { // : _MARK
  mf5F2B6B8B(); // HERE
}

void mf76D47BF1(void) { // : _>MARK
  mf5F2B6B8B(); // HERE
  mf2FE7860F(); // OVER
  mf280C9438(); // -
  mfpush(1); // $1
  mf838F7252(); // CELLS
  mf2A0C975E(); // /
  mfA2DF200E(); // SWAP
  mf240C8DEC(); // !
}

void mf79E02667(void) { // : _<MARK
  mf5F2B6B8B(); // HERE
  mf280C9438(); // -
  mfpush(1); // $1
  mf838F7252(); // CELLS
  mf2A0C975E(); // /
  mf290C95CB(); // ,
}

void mf58E8EE86(void) { // : IF
  mfat(mfdp)=(mfCell)&mfdict[228].cfa;
  mfdp+=MFSIZE; // [,] _[JMPZ]
  mf9592F3FF(); // _MARK
  mfpush(1); // $1
  mf290C95CB(); // ,
} // IMMEDIATE COMPILE-ONLY

void mf69F37330(void) { // : ELSE
  mfat(mfdp)=(mfCell)&mfdict[227].cfa;
  mfdp+=MFSIZE; // [,] _[JMP]
  mf9592F3FF(); // _MARK
  mfpush(1); // $1
  mf290C95CB(); // ,
  mfA2DF200E(); // SWAP
  mf76D47BF1(); // _>MARK
} // IMMEDIATE COMPILE-ONLY

void mfE78A4DB6(void) { // : THEN
  mf76D47BF1(); // _>MARK
} // IMMEDIATE COMPILE-ONLY

void mfD8776DB1(void) { // : CASE
  mfpush(0); // $0
} // IMMEDIATE COMPILE-ONLY

void mf88E4BCE8(void) { // : OF
  mfat(mfdp)=(mfCell)&mfdict[12].cfa;
  mfdp+=MFSIZE; // [,] OVER
  mfat(mfdp)=(mfCell)&mfdict[39].cfa;
  mfdp+=MFSIZE; // [,] =
  mf58E8EE86(); // IF
  mfat(mfdp)=(mfCell)&mfdict[8].cfa;
  mfdp+=MFSIZE; // [,] DROP
  mfA2DF200E(); // SWAP
  mf26EB3B95(); // 1+
} // IMMEDIATE COMPILE-ONLY

void mfB2F63B9B(void) { // : ENDOF
  mf67FCEB09(); // >R
  mf69F37330(); // ELSE
  mf130456D1(); // R>
} // IMMEDIATE COMPILE-ONLY

void mf75E9FDF6(void) { // : ENDCASE
  mfat(mfdp)=(mfCell)&mfdict[8].cfa;
  mfdp+=MFSIZE; // [,] DROP
mfBEGIN
  mf57E80646(); // DUP
mfWHILE
  mfA2DF200E(); // SWAP
  mf76D47BF1(); // _>MARK
  mf20EB3223(); // 1-
mfREPEAT
  mf52C16B0C(); // DROP
} // IMMEDIATE COMPILE-ONLY

void mf2AEDCB62(void) { // : RECURSE
  mfEA08BD65(); // _LATEST
  mfC50BF85F(); // @
  mf74404CD4(); // _>CFA
  mf290C95CB(); // ,
} // IMMEDIATE COMPILE-ONLY

void mf3F126D5E(void) { // : BEGIN
  mf9592F3FF(); // _MARK
} // IMMEDIATE COMPILE-ONLY

void mf9A02B1AE(void) { // : WHILE
  mf58E8EE86(); // IF
  mfA2DF200E(); // SWAP
} // IMMEDIATE COMPILE-ONLY

void mf6588C8EA(void) { // : REPEAT
  mfat(mfdp)=(mfCell)&mfdict[227].cfa;
  mfdp+=MFSIZE; // [,] _[JMP]
  mf79E02667(); // _<MARK
  mf76D47BF1(); // _>MARK
} // IMMEDIATE COMPILE-ONLY

void mf64F9C7EF(void) { // : UNTIL
  mfat(mfdp)=(mfCell)&mfdict[228].cfa;
  mfdp+=MFSIZE; // [,] _[JMPZ]
  mf79E02667(); // _<MARK
} // IMMEDIATE COMPILE-ONLY

void mf5E7C87EF(void) { // : AGAIN
  mfat(mfdp)=(mfCell)&mfdict[227].cfa;
  mfdp+=MFSIZE; // [,] _[JMP]
  mf79E02667(); // _<MARK
} // IMMEDIATE COMPILE-ONLY

void mfDA088408(void) { // : _[LOOP]
  mfCell ofs=mftos,pr=mfrtos,po=pr+ofs; 
  mftos=!(ofs<0?(po<0)&&(pr>=0):(po>=0)&&(pr<0)); 
  mfrtos=po;
}

void mfC0946685(void) { // : _[DO]
  mfA2DF200E(); // SWAP
  mf67FCEB09(); // >R
  mf7D04FDAF(); // R@
  mf280C9438(); // -
  mf67FCEB09(); // >R
  mfpush(0); // $0
}

void mfE2A5025C(void) { // : _[?DO]
  mfC0946685(); // _[DO]
  mf52C16B0C(); // DROP
  mf7D04FDAF(); // R@
  mf14ED5DD6(); // 0=
}

void mf34D794DE(void) { // : __DO
  mf9592F3FF(); // _MARK
  mfat(mfdp)=(mfCell)&mfdict[245].cfa;
  mfdp+=MFSIZE; // [,] _[LOOP]
  mf58E8EE86(); // IF
  mfA2DF200E(); // SWAP
  mfpush(0); // $0
}

void mf41CE86D4(void) { // : DO
  mfat(mfdp)=(mfCell)&mfdict[246].cfa;
  mfdp+=MFSIZE; // [,] _[DO]
  mf34D794DE(); // __DO
} // IMMEDIATE COMPILE-ONLY

void mfEC084DA3(void) { // : ?DO
  mfat(mfdp)=(mfCell)&mfdict[247].cfa;
  mfdp+=MFSIZE; // [,] _[?DO]
  mf34D794DE(); // __DO
} // IMMEDIATE COMPILE-ONLY

void mfCC0C0364(void) { // : I
  mferr(mfrin(2);mfout(1)); 
  mfpush(mfrtos+mfrsec);
} // COMPILE-ONLY

void mfCF0C081D(void) { // : J
  mferr(mfrin(4);mfout(1)); 
  mfpush(mfrp[-2]+mfrp[-3]);
} // COMPILE-ONLY

void mfE11A6788(void) { // : UNLOOP
  mferr(mfrin(3)); 
  mfrp-=2;
} // COMPILE-ONLY

void mf354C36E0(void) { // : LEAVE
  mfpush(0); // $0
  mfBAFC44C4(); // LITERAL
  mfat(mfdp)=(mfCell)&mfdict[227].cfa;
  mfdp+=MFSIZE; // [,] _[JMP]
  mfCell *s=mfsp,a=0,f=-1;    
  while(s>mfstk){if(!*s){a=*(s-1)+MFSIZE,f=0;break;}s--;};   
  mfpush(a); mfpush(f);    
mfIF
  mfpush(-22); // $FFFFFFEA
  (MFE5B4B40F)(); // THROW
mfTHEN
  mf79E02667(); // _<MARK
} // IMMEDIATE COMPILE-ONLY

void mfF033FF78(void) { // : +LOOP
  mf52C16B0C(); // DROP
  mf6588C8EA(); // REPEAT
  mfat(mfdp)=(mfCell)&mfdict[252].cfa;
  mfdp+=MFSIZE; // [,] UNLOOP
} // IMMEDIATE COMPILE-ONLY

void mfDBE07C6B(void) { // : LOOP
  mfpush(1); // $1
  mfBAFC44C4(); // LITERAL
  mfF033FF78(); // +LOOP
} // IMMEDIATE COMPILE-ONLY

void mf926854F5(void) { // : _[LOOP]_
   mfCell ofs=mftos, pr=mfsec, po=pr+ofs; 
   mfrtos+=ofs, mfrsec=po, mftos=(pr^po)<0;
}

void mf039DE92E(void) { // : _[DO]_
  mf6B09DC74(); // _MSB
  mf87E8362E(); // ROT
  mf280C9438(); // -
  mf67FCEB09(); // >R
  mf67FCEB09(); // >R
  mfpush(0); // $0
}

void mfCCC22AB9(void) { // : _[?DO]_
  mfE0839FF6(); // 2DUP
  mf2FE7860F(); // OVER
  mf380CAD68(); // =
  mf89F60699(); // 2>R
  mfC0946685(); // _[DO]
  mf34FF0561(); // 2R>
mfIF
  mfA2DF200E(); // SWAP
mfTHEN
  mf52C16B0C(); // DROP
}

void mfB05EC713(void) { // : __DO_
  mf9592F3FF(); // _MARK
  mfat(mfdp)=(mfCell)&mfdict[256].cfa;
  mfdp+=MFSIZE; // [,] _[LOOP]_
  mf58E8EE86(); // IF
  mfA2DF200E(); // SWAP
  mfpush(0); // $0
}

void mf231DCCD1(void) { // : DO_
  mfat(mfdp)=(mfCell)&mfdict[257].cfa;
  mfdp+=MFSIZE; // [,] _[DO]_
  mfB05EC713(); // __DO_
} // IMMEDIATE COMPILE-ONLY

void mf8D12C3B4(void) { // : ?DO_
  mfat(mfdp)=(mfCell)&mfdict[258].cfa;
  mfdp+=MFSIZE; // [,] _[?DO]_
  mfB05EC713(); // __DO_
} // IMMEDIATE COMPILE-ONLY

void mf71E915E1(void) { // : I_
  mf7D04FDAF(); // R@
} // COMPILE-ONLY

void mf31F0FFE6(void) { // : J_
  mfpush(mfrp[-3]);
} // COMPILE-ONLY

void mf3CF98E2B(void) { // : _[VAR]
  mferr(mfout(1)); 
  mfpush(mfw+1);
}

void mf04B69205(void) { // : VARIABLE
  mfpush(&mfdict[263].cfa); // ['] _[VAR]
  mfC94E167C(); // _PARSE-WORD
  mf20B9677F(); // _HEADER
  mfpush(0); // $0
  mf290C95CB(); // ,
  mf3BFC9593(); // _REVEAL
}

void mf5047FCD5(void) { // : _[CONST]
  mferr(mfout(1)); 
  mfpush(mfat(mfw+1));
}

void mfADA9DCA5(void) { // : CONSTANT
  mfpush(&mfdict[265].cfa); // ['] _[CONST]
  mfC94E167C(); // _PARSE-WORD
  mf20B9677F(); // _HEADER
  mf290C95CB(); // ,
  mf3BFC9593(); // _REVEAL
}

void mfCD566A47(void) { // : _[BUF]
  mferr(mfout(1)); 
  mfpush(mfw+2);
}

void mf86AC1293(void) { // : BUFFER:
  mfpush(&mfdict[267].cfa); // ['] _[BUF]
  mfC94E167C(); // _PARSE-WORD
  mf20B9677F(); // _HEADER
  mf57E80646(); // DUP
  mf290C95CB(); // ,
  mf3FA1943F(); // ALLOT
  mfAFA183BE(); // ALIGN
  mf3BFC9593(); // _REVEAL
}

void mfCDFDC67E(void) { // : _[CREATE]
  mferr(mfout(2)); 
  mfpush(mfw+2), mfpush(mfat(mfw+1)); 
  mfCF77DFB8(); // EXECUTE
}

void mfC19230DD(void) { // : CREATE
  mfpush(&mfdict[269].cfa); // ['] _[CREATE]
  mfC94E167C(); // _PARSE-WORD
  mf20B9677F(); // _HEADER
  mfat(mfdp)=(mfCell)&mfdict[25].cfa;
  mfdp+=MFSIZE; // [,] NOOP
  mf3BFC9593(); // _REVEAL
}

void mf4520460B(void) { // : _<DOES
  mfEA08BD65(); // _LATEST
  mfC50BF85F(); // @
  mf74404CD4(); // _>CFA
  mfBB8FCA7A(); // CELL+
  mf240C8DEC(); // !
}

void mfFF60370A(void) { // : DOES>
  mf3FC95052(); // __CSYS?
  mf04316999(); // __CSYS
  mf5F2B6B8B(); // HERE
  mfpush(4); // $4
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
  mfBAFC44C4(); // LITERAL
  mfat(mfdp)=(mfCell)&mfdict[271].cfa;
  mfdp+=MFSIZE; // [,] _<DOES
  mfpush(0); // $0
  mf290C95CB(); // ,
  mfpush(&mfdict[206].cfa); // ['] _[:]
  mfC50BF85F(); // @
  mf290C95CB(); // ,
} // IMMEDIATE COMPILE-ONLY

void mf800A2691(void) { // : >BODY
  mf57E80646(); // DUP
  mfC50BF85F(); // @
  mfpush(&mfdict[269].cfa); // ['] _[CREATE]
  mfC50BF85F(); // @
  mf380CAD68(); // =
mfIF
  mfBB8FCA7A(); // CELL+
mfTHEN
  mfBB8FCA7A(); // CELL+
}

void mf9F67357B(void) { // : _>V+
  mfBB8FCA7A(); // CELL+
  mfC50BF85F(); // @
}

void mf99672C09(void) { // : _>V!
  mfpush(2); // $2
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
  mfC50BF85F(); // @
}

void mf38669356(void) { // : _>V@
  mfpush(3); // $3
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
  mfC50BF85F(); // @
}

void mf3C6699A2(void) { // : _>VD
  mfpush(4); // $4
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
}

void mf80B951E1(void) { // : _[VAL]
  mferr(mfout(1)); 
  mfpush(mfw+4), mfw=(mfXT*)mfat(mfw+3); (*mfw)();
}

void mf24D3F916(void) { // : _(VALUE)
  mfpush(&mfdict[278].cfa); // ['] _[VAL]
  mfC94E167C(); // _PARSE-WORD
  mf20B9677F(); // _HEADER
  mf290C95CB(); // ,
  mf290C95CB(); // ,
  mf290C95CB(); // ,
  mf3BFC9593(); // _REVEAL
}

void mf2513E22A(void) { // : VALUE
  mfpush(&mfdict[82].cfa); // ['] @
  mfpush(&mfdict[83].cfa); // ['] !
  mfpush(&mfdict[45].cfa); // ['] +
  mf24D3F916(); // _(VALUE)
  mf290C95CB(); // ,
}

void mfCCCCC70B(void) { // : __?VAL
  mf220C8AC6(); // '
  mf57E80646(); // DUP
  mfC50BF85F(); // @
  mfpush(&mfdict[278].cfa); // ['] _[VAL]
  mfC50BF85F(); // @
  mf380CAD68(); // =
mfNOTIF
  mfpush(-32); // $FFFFFFE0
  (MFE5B4B40F)(); // THROW
mfTHEN
}

void mf21F563E4(void) { // : TO
  mfCCCCC70B(); // __?VAL
  mf57E80646(); // DUP
  mf3C6699A2(); // _>VD
  mfA2DF200E(); // SWAP
  mf99672C09(); // _>V!
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
mfIF
  mfA2DF200E(); // SWAP
  mfBAFC44C4(); // LITERAL
  mf290C95CB(); // ,
mfELSE
  mfCF77DFB8(); // EXECUTE
mfTHEN
} // IMMEDIATE 

void mfBA65A38C(void) { // : _[DEFER]
  mfw=(mfXT*)(mfat(mfw-2)&-4); (*mfw)();
}

void mfDC2E9701(void) { // : DEFER@
  mfC439E65E(); // _<CFA
  mf4B2037E9(); // _>PFA
  mfC50BF85F(); // @
  mfpush(-4); // $FFFFFFFC
  mf91666DC6(); // AND
}

void mf3B2F2C8E(void) { // : DEFER!
  mfC439E65E(); // _<CFA
  mf4B2037E9(); // _>PFA
  mfA2DF200E(); // SWAP
  mf57E80646(); // DUP
  mfC439E65E(); // _<CFA
  mf4B2037E9(); // _>PFA
  mfC50BF85F(); // @
  mfpush(3); // $3
  mf91666DC6(); // AND
  mf7CE4AA04(); // OR
  mfA2DF200E(); // SWAP
  mf240C8DEC(); // !
}

void mfE9EDB1DB(void) { // : DEFER
  mfpush(&mfdict[282].cfa); // ['] _[DEFER]
  mfC94E167C(); // _PARSE-WORD
  mf20B9677F(); // _HEADER
  mfpush(&mfdict[25].cfa); // ['] NOOP
  mf5F2B6B8B(); // HERE
  mfpush(1); // $1
  mf838F7252(); // CELLS
  mf280C9438(); // -
  mf3B2F2C8E(); // DEFER!
  mf3BFC9593(); // _REVEAL
}

void mfCB746F0B(void) { // : __?DEF
  mf220C8AC6(); // '
  mf57E80646(); // DUP
  mfC50BF85F(); // @
  mfpush(&mfdict[282].cfa); // ['] _[DEFER]
  mfC50BF85F(); // @
  mf93F7201F(); // <>
mfIF
  mfpush(-32); // $FFFFFFE0
  (MFE5B4B40F)(); // THROW
mfTHEN
}

void mf55EA9727(void) { // : ACTION-OF
  mfCB746F0B(); // __?DEF
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
mfIF
  mfBAFC44C4(); // LITERAL
  mfat(mfdp)=(mfCell)&mfdict[283].cfa;
  mfdp+=MFSIZE; // [,] DEFER@
mfELSE
  mfDC2E9701(); // DEFER@
mfTHEN
} // IMMEDIATE 

void mf6DE90F95(void) { // : IS
  mfCB746F0B(); // __?DEF
  mfpush(&MFE09A6F96); // STATE
  mfC50BF85F(); // @
mfIF
  mfBAFC44C4(); // LITERAL
  mfat(mfdp)=(mfCell)&mfdict[284].cfa;
  mfdp+=MFSIZE; // [,] DEFER!
mfELSE
  mf3B2F2C8E(); // DEFER!
mfTHEN
} // IMMEDIATE 

void mf55174F30(void) { // : _[MARKER]
  mfC50BF85F(); // @
  mf57E80646(); // DUP
  mf57E80646(); // DUP
  mfC50BF85F(); // @
  mfB8F49A42(); // _LAST
  mf240C8DEC(); // !
  mfBB8FCA7A(); // CELL+
  mf57E80646(); // DUP
  mfC50BF85F(); // @
  mfEA08BD65(); // _LATEST
  mf240C8DEC(); // !
  mfBB8FCA7A(); // CELL+
  mf57E80646(); // DUP
  mfC50BF85F(); // @
  mfE345B847(); // _CURRENT
  mf240C8DEC(); // !
  mfBB8FCA7A(); // CELL+
  mf57E80646(); // DUP
  mf82CE5579(); // _CONTEXT
  mfpush(8); // $8
  mf838F7252(); // CELLS
  mfC2064154(); // MOVE
  mfpush(8); // $8
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
  mf85A4C44F(); // _WORDLISTS
  mfpush(20); // $14
  mf838F7252(); // CELLS
  mfC2064154(); // MOVE
  mfD148A1CA(); // _DP
  mf240C8DEC(); // !
}

void mfB53F8A17(void) { // : MARKER
  mf5F2B6B8B(); // HERE
  mfB8F49A42(); // _LAST
  mfC50BF85F(); // @
  mf290C95CB(); // ,
  mfEA08BD65(); // _LATEST
  mfC50BF85F(); // @
  mf290C95CB(); // ,
  mfE345B847(); // _CURRENT
  mfC50BF85F(); // @
  mf290C95CB(); // ,
  mf82CE5579(); // _CONTEXT
  mf5F2B6B8B(); // HERE
  mfpush(8); // $8
  mf838F7252(); // CELLS
  mf57E80646(); // DUP
  mf3FA1943F(); // ALLOT
  mfC2064154(); // MOVE
  mf85A4C44F(); // _WORDLISTS
  mf5F2B6B8B(); // HERE
  mfpush(20); // $14
  mf838F7252(); // CELLS
  mf57E80646(); // DUP
  mf3FA1943F(); // ALLOT
  mfC2064154(); // MOVE
  mfC19230DD(); // CREATE
  mf290C95CB(); // ,
  mfpush(&mfdict[288].cfa); // ['] _[MARKER]
  mf4520460B(); // _<DOES
}

void mf304F16BF(void) { // : ENVIRONMENT?
  mfE0839FF6(); // 2DUP
  mfspush("/COUNTED-STRING"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(255); // $FF
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("/HOLD"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(21); // $15
  mf838F7252(); // CELLS
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("/PAD"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(256); // $100
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("ADDRESS-UNIT-BITS"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(8); // $8
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("FLOORED"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(MFEE597878); // FALSE
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("MAX-CHAR"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(255); // $FF
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("MAX-D"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(-1); // $FFFFFFFF
  mfpush(INTPTR_MAX);
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("MAX-N"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(INTMAX_MAX);
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("MAX-UD"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(-1); // $FFFFFFFF
  mfpush(-1); // $FFFFFFFF
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("MAX-U"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(-1); // $FFFFFFFF
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("RETURN-STACK-CELLS"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(MFRSTSIZE);
  mfpush(-1); // $FFFFFFFF
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("STACK-CELLS"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(MFSTKSIZE);
  mfpush(-1); // $FFFFFFFF
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("CORE"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(MF501B0925); // TRUE
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfE0839FF6(); // 2DUP
  mfspush("CORE-EXT"); // S"
  mf140219CD(); // S=
mfIF
  mfEF6476DC(); // 2DROP
  mfpush(MF501B0925); // TRUE
  mfpush(MF501B0925); // TRUE
mfEXIT
mfTHEN
  mfEF6476DC(); // 2DROP
  mfpush(MFEE597878); // FALSE
}

void mf9A18E8D5(void) { // : FLOAD
  char fbuf[128]; int l, ln=0, xc=0; 
  mfF92179F8(); // PARSE-NAME
  mfpush(fbuf);
  mf10E38971(); // _PLACE
  FILE* h=fopen(fbuf+1,"r"); if (!h) mfthrow(-38); 
  mf600421B4(); // _SRC>R
  mfpush(h);
  mfFD898658(); // _SRCID!
  mfpush(0); // $0
  mfEF18D029(); // _BLK
  mf240C8DEC(); // !
  while (fgets(fbuf,128,h)) { 
    ln+=1, l=strlen(fbuf), l-=fbuf[l-1]==0xA, mfpush(fbuf), mfpush(l); 
  mf5AF98C79(); // _SRC2!
  mfpush(0); // $0
  mf580B1E42(); // >IN
  mf240C8DEC(); // !
  mfpush(&mfdict[176].cfa); // ['] _INTERPRET
  mfEAF28AAC(); // CATCH
mfDUPIF
  mf9FC8664E(); // _EXCSRC
  mfprint("in file line "); // S"
  mfprintn(ln);
mfTHEN
    xc=mfpop(); if (xc) break; } 
  mfD24E2A14(); // _R>SRC
  fclose(h); mfthrow(xc);
}

void mfC6897287(void) { // : DUMP
  mfpush(4095); // $FFF
  mf91666DC6(); // AND
  mfpush(&MFE972DB58); // BASE
  mfC50BF85F(); // @
  mf87E8362E(); // ROT
  mf87E8362E(); // ROT
  mf818F192A(); // HEX
  mf2FE7860F(); // OVER
  mf2E0C9DAA(); // +
  mf2FE7860F(); // OVER
  mfpush(-16); // $FFFFFFF0
  mf91666DC6(); // AND
  mf64D9F6E0(); // CR
  mfprint("    Address:   "); // S"
  mfpush(16); // $10
mfFOR
mfN
  mf20F323BA(); // U.
  mf1808F0E5(); // SPACE
mfNEXT
  mfprint("0123456789ABCDEF"); // S"
mfBEGIN
  mfE0839FF6(); // 2DUP
  mf10F30A8A(); // U>
mfWHILE
  mf64D9F6E0(); // CR
  mf57E80646(); // DUP
  mfpush(12); // $C
  mfC6C18638(); // U.R
  mf1808F0E5(); // SPACE
  mfpush(124); // $7C
  mfD9BC5E8A(); // EMIT
  mf57E80646(); // DUP
  mf67FCEB09(); // >R
  mfpush(16); // $10
mfBEGIN
  mfA2DF200E(); // SWAP
  mf8C8BC01F(); // 2OVER
  mfpush(2); // $2
  mfE67669F8(); // PICK
  mf87E8362E(); // ROT
  mf87E8362E(); // ROT
  mf09D242EA(); // WITHIN
mfIF
  mf57E80646(); // DUP
  mf76DA1336(); // C@
  mfpush(0); // $0
  mfA6F73E08(); // <#
  mf260C9112(); // #
  mf260C9112(); // #
  mf11C88844(); // #>
mfELSE
  mfspush("  "); // S"
mfTHEN
  mf142FE78D(); // TYPE
  mf26EB3B95(); // 1+
  mfA2DF200E(); // SWAP
  mf20EB3223(); // 1-
  mf57E80646(); // DUP
  mfpush(4); // $4
  mf6453F3A3(); // MOD
mfIF
  mfpush(MF20DD5D6B); // BL
mfELSE
  mfpush(124); // $7C
mfTHEN
  mfD9BC5E8A(); // EMIT
  mf57E80646(); // DUP
mfNOTUNTIL
  mfEF6476DC(); // 2DROP
  mf1808F0E5(); // SPACE
  mf130456D1(); // R>
  mfpush(16); // $10
mfBEGIN
  mfA2DF200E(); // SWAP
  mf8C8BC01F(); // 2OVER
  mfpush(2); // $2
  mfE67669F8(); // PICK
  mf87E8362E(); // ROT
  mf87E8362E(); // ROT
  mf09D242EA(); // WITHIN
mfIF
  mf57E80646(); // DUP
  mf76DA1336(); // C@
  mf57E80646(); // DUP
  mfpush(MF20DD5D6B); // BL
  mfpush(128); // $80
  mf09D242EA(); // WITHIN
  mf14ED5DD6(); // 0=
mfIF
  mf52C16B0C(); // DROP
  mfpush(46); // $2E
mfTHEN
  mfD9BC5E8A(); // EMIT
mfELSE
  mf1808F0E5(); // SPACE
mfTHEN
  mf26EB3B95(); // 1+
  mfA2DF200E(); // SWAP
  mf20EB3223(); // 1-
  mf57E80646(); // DUP
mfNOTUNTIL
  mf52C16B0C(); // DROP
mfREPEAT
  mfEF6476DC(); // 2DROP
  mf52C16B0C(); // DROP
  mfpush(&MFE972DB58); // BASE
  mf240C8DEC(); // !
}

void mf07F5E02A(void) { // : WORDS
  mf64D9F6E0(); // CR
  mf82CE5579(); // _CONTEXT
  mfC50BF85F(); // @
  mfC50BF85F(); // @
mfBEGIN
  mf57E80646(); // DUP
  mfC50BF85F(); // @
mfWHILE
  mf57E80646(); // DUP
  mf03D7EB73(); // _>NAME
  mfA3F7B2D4(); // COUNT
  mf142FE78D(); // TYPE
  mf1808F0E5(); // SPACE
  mfC50BF85F(); // @
mfREPEAT
  mf52C16B0C(); // DROP
}

mfCell MFCDA5B50C=0; // 0

void mfCDA5B50C(void) { // VARIABLE _STDS
  mfpush(&MFCDA5B50C);
}

mfCell MFDBA5CB16=0; // 0

void mfDBA5CB16(void) { // VARIABLE _STDE
  mfpush(&MFDBA5CB16);
}

mfCell MF5BF2539C=-1; // -1

void mf5BF2539C(void) { // VARIABLE _VERBOSE
  mfpush(&MF5BF2539C);
}

void mf1E9174EB(void) { // : _ATB
  mf9F4C117D(); // _LIMIT
  mfpush(20); // $14
  mf838F7252(); // CELLS
  mf280C9438(); // -
}

void mfF959CA7B(void) { // : TESTING
  mfB9790298(); // SOURCE
  mfpush(&MF5BF2539C); // _VERBOSE
  mfC50BF85F(); // @
mfIF
  mf57E80646(); // DUP
  mf67FCEB09(); // >R
  mf142FE78D(); // TYPE
  mf64D9F6E0(); // CR
  mf130456D1(); // R>
  mf580B1E42(); // >IN
  mf240C8DEC(); // !
mfELSE
  mf580B1E42(); // >IN
  mf240C8DEC(); // !
  mf52C16B0C(); // DROP
  mfpush(42); // $2A
  mfD9BC5E8A(); // EMIT
mfTHEN
}

void mf4DF5A928(void) { // : T{
  mfCE61558A(); // DEPTH
  mfpush(&MFCDA5B50C); // _STDS
  mf240C8DEC(); // !
}

void mf11CD0572(void) { // : ->
  mfCE61558A(); // DEPTH
  mfpush(&MFDBA5CB16); // _STDE
  mf240C8DEC(); // !
  mf1E9174EB(); // _ATB
  mfpush(&MFDBA5CB16); // _STDE
  mfC50BF85F(); // @
  mfpush(&MFCDA5B50C); // _STDS
  mfC50BF85F(); // @
  mf280C9438(); // -
  mfpush(20); // $14
  mf0E45F4B7(); // MIN
  mfpush(1); // $1
  mf838F7252(); // CELLS
mfFORUP
mfN
  mf240C8DEC(); // !
mfNEXT
}

void mf4DC062EA(void) { // : __}T
  mf64D9F6E0(); // CR
  mfB9790298(); // SOURCE
  mf142FE78D(); // TYPE
  mf142FE78D(); // TYPE
}

void mf7756E4B4(void) { // : }T
  mfCE61558A(); // DEPTH
  mfpush(&MFDBA5CB16); // _STDE
  mfC50BF85F(); // @
  mf280C9438(); // -
  mf57E80646(); // DUP
  mf15ED5F69(); // 0<
mfIF
  mfspush(" ?? too many stack elements"); // S"
  mf4DC062EA(); // __}T
  mf367DE7D9(); // ABORT
mfTHEN
  mf13ED5C43(); // 0>
mfIF
  mfspush(" ?? too few stack elements"); // S"
  mf4DC062EA(); // __}T
  mf367DE7D9(); // ABORT
mfTHEN
  mfpush(&MFDBA5CB16); // _STDE
  mfC50BF85F(); // @
  mfpush(&MFCDA5B50C); // _STDS
  mfC50BF85F(); // @
  mf280C9438(); // -
  mfpush(20); // $14
  mf0E45F4B7(); // MIN
mfFOR
  mf1E9174EB(); // _ATB
mfN
  mf838F7252(); // CELLS
  mf2E0C9DAA(); // +
  mfC50BF85F(); // @
  mf93F7201F(); // <>
mfIF
  mfspush(" ?? wrong stack element #"); // S"
  mf4DC062EA(); // __}T
  mfpush(&MFDBA5CB16); // _STDE
  mfC50BF85F(); // @
  mfpush(&MFCDA5B50C); // _STDS
  mfC50BF85F(); // @
  mf280C9438(); // -
mfN
  mf280C9438(); // -
  mf2B0C98F1(); // .
  mf367DE7D9(); // ABORT
mfTHEN
mfNEXT
}

void mf8E9972D8(void) { // : _TICKER
  mferr(mfout(1)); 
  mfpush((1000*clock())/CLOCKS_PER_SEC);
}

mfCell MF96116CF3=0; // 0

void mf96116CF3(void) { // VARIABLE _TMFL
  mfpush(&MF96116CF3);
}

mfCell MF889600AF=0; // 0

void mf889600AF(void) { // VARIABLE _TIMER
  mfpush(&MF889600AF);
}

void mfFAA3E0E8(void) { // : TIMER-RESET
  mfpush(-1); // $FFFFFFFF
  mfpush(&MF96116CF3); // _TMFL
  mf240C8DEC(); // !
  mf8E9972D8(); // _TICKER
  mfpush(&MF889600AF); // _TIMER
  mf240C8DEC(); // !
}

void mf2F77738D(void) { // : TIMER-STOP
  mfpush(0); // $0
  mfpush(&MF96116CF3); // _TMFL
  mf240C8DEC(); // !
  mf8E9972D8(); // _TICKER
  mfpush(&MF889600AF); // _TIMER
  mfC50BF85F(); // @
  mf280C9438(); // -
  mfpush(&MF889600AF); // _TIMER
  mf240C8DEC(); // !
}

void mfC3A5A6AF(void) { // : .ELAPSED
  mfpush(&MF96116CF3); // _TMFL
  mfC50BF85F(); // @
mfIF
  mf2F77738D(); // TIMER-STOP
mfTHEN
  mfpush(&MF889600AF); // _TIMER
  mfC50BF85F(); // @
  mf2B0C98F1(); // .
  mfprint("ms"); // S"
}

void mf96272888(void) { // : MAIN
  mf28B3832A(); // _BOOT
  mf49997C51(); // _LOGO
  mf946BA378(); // _ABORT
  mfB6A8BBC3(); // BYE
}

// --- C Main ---

int main(int argc, char* argv[]){
  mfargc=argc, mfargv=argv;
  mfTHROW=mfE5B4B40F;
  setjmp(mfabort);
  mf96272888(); // MAIN
}

// --- End ---
