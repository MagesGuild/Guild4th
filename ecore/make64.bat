@echo off

call tc64.bat emf2c

emf2c

call tc64.bat emf3

if exist emf64.exe del emf64.exe
ren emf3.exe emf64.exe
echo Renamed emf3.exe to emf64.exe
echo.
echo Ready to start emf64 and enjoy your new
echo MinForth V3.4e 64-bit kernel :-)
echo.
pause
cls
emf64
