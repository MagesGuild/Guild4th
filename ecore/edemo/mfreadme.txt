--- MinForth V3.4e experimental core tests

build transpiler
Win10 : gc64 emf2c
Linux : ./lmake emf2c

transpile ecore
Win10 : emf2c
Linux : ./emf2c

compile ecore
Win10 : gc64 emf3
Linux : ./lmake emf3

run mf ecore
Win10 : emf3
Linux : ./emf3

--- kernel test

cd test
..\emf3

MinForth V3.4e
# fload runtests.mf
# bye

--- demo progs

run sieve benchmark from ecore
emf3 ( Linux: ./emf3 )

MinForth V3.4e
# fload sieve.mf
# bye

transpile sieve benchmark ( file sieve.mfc )
Win10 : emf2c sieve
Linux : ./emf2c sieve
-> resulting > 90k C program with Forth kernel included

compile sieve benchmark
Win10 : gc64 sieve
Linux : ./lmake sieve

strip off unused words
uncomment #REDUCE in file sieve.mfc
transpiles to small > 10k C program without Forth kernel

--- CPU 6502 emulator

emf3 ( Linux: ./emf3 )

MinForth V3.4e
# fload 6502.mf
# bye

transpile 6502 emulator ( file 6502.mfc )
Win10 : emf2c 6502
Linux : ./emf2c 6502

compile 6502 emulator
Win10 : gc64 6502
Linux : ./lmake 6502

6502 ( Linux: ./6502 )
