@echo off

call tc32.bat emf2c

emf2c

call tc32.bat emf3

if exist emf32.exe del emf32.exe
ren emf3.exe emf32.exe
echo Renamed emf3.exe to emf32.exe
echo.
echo Ready to start emf32 and enjoy your new
echo MinForth V3.4e 32-bit kernel :-)
echo.
pause
cls
emf32
