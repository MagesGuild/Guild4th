@rem ***********************************************
@rem *  Compiling MinForth from the command line   *
@rem *  TCC Compiler                               *
@rem ***********************************************
@echo off

rem Set your compiler path here
set CC=..\src\tcc\

if not _%1%_ == __ goto makemf
echo Usage:  tc32 program (without suffix)
echo f.ex.:  tc32 kernel  (without .c)
goto end

:makemf
echo Making %1%.exe with tcc 32 bit

set FILE=%1%
set OPATH=%PATH%
set PATH=%CC%;%OPATH%

rem For 32/64 bit append -m32/-m64  
set OPT=-Wall -m32 -funsigned-char

tcc %OPT% %FILE%.c -o %FILE%.exe

set OPT=
set PATH=%OPATH%
set OPATH=
set CC=

:end
:pause
