@rem ***********************************************
@rem *  Compiling MinForth from the command line   *
@rem *  Clang Compiler                             *
@rem ***********************************************
@echo off

rem Set your compiler path here
rem set CC=d:\develop\llvm\bin

if not _%1%_ == __ goto makemf
echo Usage:  cl64 program (without suffix)
echo f.ex.:  cl64 kernel  (without .c)
goto end


:makemf

REM Clang installed along with Visual Studio:
set CC="c:\program files (x86)\Microsoft Visual Studio\2019\BuildTools"
call %CC%\VC\Auxiliary\Build\vcvarsall.bat x64

set FILE=%1%
set OPATH=%PATH%
set PATH=%CC%;%OPATH%
  
set OPT=-Wall -Wextra -std=c99 -m64 -funsigned-char

echo Making %1%.exe with clang 64-bit

clang %OPT% %FILE%.c -o %FILE%.exe

set OPT=
set PATH=%OPATH%
set OPATH=
set CC=

:end
pause
