@echo off

call tc32.bat mf2c

mf2c

call tc32.bat mf3

if exist mf32.exe del mf32.exe
copy mf3.exe mf32.exe
echo Copied mf3.exe to mf32.exe
echo.
echo Ready to start mf32 and enjoy your new
echo MinForth V3.4 32-bit :-)
echo.
pause
cls
mf32
