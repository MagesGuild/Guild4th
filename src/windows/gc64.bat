@rem ***********************************************
@rem *  Compiling MinForth fron the command line   *
@rem *  MinGW-W64 Compiler                         *
@rem ***********************************************
@echo off

rem Set your compiler path here
set CC=d:\develop\mingw-w64\mingw64\bin

if not _%1%_ == __ goto makemf
echo Usage:  gc64 program (without suffix)
echo f.ex.:  gc64 mf2c    (without .c)
goto end

:makemf
echo Making %1%.exe with MinGW-W64

set FILE=%1%
set OPATH=%PATH%
set PATH=%CC%;%OPATH%

rem For 32/64 bit use -m32/-m64  
set OPT=-Wall -Wextra -std=c99 -m64 -O -funsigned-char -D__USE_MINGW_ANSI_STDIO

gcc %OPT% %FILE%.c -o %FILE%.exe

if exist %FILE%.exe strip %FILE%.exe

set OPT=
set PATH=%OPATH%
set OPATH=
set CC=

:end
pause
