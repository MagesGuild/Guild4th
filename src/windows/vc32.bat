@rem *********************************************
@rem *  Compiling MinForth via the command line  *
@rem *  Visual Studio 2019 Build Tools           *
@rem *********************************************
@echo off

rem Set your bUILD tOOLS compiler path here
set CC="c:\program files (x86)\Microsoft Visual Studio\2019\BuildTools"

rem Example for 2017 Community Edition
rem set CC="c:\program files (x86)\Microsoft Visual Studio\2017\Community"

if not _%1%_ == __ goto makemf
echo Usage:  vc32 program (without suffix)
echo f.ex.:  vc32 mf2c    (without .c)
goto end		

:makemf
echo Making %1%.exe with Visual C++ 32-bit

rem For 32/64 bit use x86/x64  
call %CC%\VC\Auxiliary\Build\vcvarsall.bat x86
	
set FILE=%1%
set OPT=/nologo /J

cl %OPT% %FILE%.c

if exist %FILE%.obj del %FILE%.obj

set FILE=
set OPT=
set CC=

:end
pause
