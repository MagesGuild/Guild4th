@echo off

call tc64.bat mf2c

mf2c

call tc64.bat mf3

if exist mf64.exe del mf64.exe
copy mf3.exe mf64.exe
echo Copied mf3.exe to mf64.exe
echo.
echo Ready to start mf64 and enjoy your new
echo MinForth V3.4 64-bit :-)
echo.
pause
cls
mf64
