MinForth V3.4 on Windows
------------------------

Linux usere can delete this directory.

Use the files within this directory as batch script template
to call your preferred C compiler. Copy your preferred
batch template to your MF34\src directory and adapt compiler
paths within the batch file to suit your system.

Test it by building the famous hello program
(example for MinGW 32-bit)
  gc32 hello
  hello

Usage example to build MinForth with clang
  cl64 mf2c
  mf2c
  cl64 mf3
  mf3

If you don't use C for other purposes and don't want to
install hundreds of megabytes, Tiny C is recommended.
It should already be available in you MF34\src\tcc directory.
 
( Note: If not, download the latest win64 version from
  http://download.savannah.gnu.org/releases/tinycc/ 
  and unzip the files into MinForth's src/tcc directory.
  If you are working on 32-bit Windows, you'll want the
  win32 tcc version.)

Use tc32.bat or tc64.bat to compile C files with tcc,
as with these command line entries: 
  tc32 mf2c
  mf2c
  tc32 mf3
  mf3

For more IDE-support, Pelles C can be recommended.
http://www.smorgasbordet.com/pellesc/
Install and start it, then open the enclosed MF3.ppw
project file.
