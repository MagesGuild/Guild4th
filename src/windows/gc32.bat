@rem ***********************************************
@rem *  Compiling MinForth fron the command line   *
@rem *  MinGW Compiler                             *
@rem ***********************************************
@echo off

rem Set your compiler path here
set CC=d:\develop\mingw\bin

if not _%1%_ == __ goto makemf
echo Usage:  gc32 program (without suffix)
echo f.ex.:  gc32 kernel  (without .c)
goto end

:makemf
echo Making %1%.exe with MinGW-W32

set FILE=%1%
set OPATH=%PATH%
set PATH=%CC%;%OPATH%
set OPT=-Wall -Wextra -funsigned-char

gcc %OPT% %FILE%.c -o %FILE%.exe

if exist %FILE%.exe strip %FILE%.exe

set OPT=
set PATH=%OPATH%
set OPATH=
set CC=

:end
pause
