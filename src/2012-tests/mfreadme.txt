MinForth V3.4 Testsuite Results
-------------------------------

Run the tests from within MinForth with
> include runtests.fth
in fp directory
> include runfptests.fth
> include paranoia.4th

In fact MinForth passes all Forth-2012 standard tests. Some remarks to
reported errors:

a) Block wordset test shows 2 false errors because the test assumes BUFFER
and BLOCK to be identical. This is obviously questionable.
(there is also a pertaining comment in gforth source file blocks.fs)

b) String wordset test shows 4 pseudo-errors in the SUBSTITUTE tests that can
be attributed to over-zealous testing of undefined behaviour.

c) Some C compilers might cause floating-point number accuracy warnings.
Clang, gcc and MSVCC cause no warnings.

d) The paranoia test should be run separately from the other fp tests
because they redefine i.e. 'overwrite' some words internally which can
cause false error report.
