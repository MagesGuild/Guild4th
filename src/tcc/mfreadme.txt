MinForth V3.4 TCC Compiler Usage
--------------------------------

Linux users can delete this directory.

Download the latest tcc-win64 version from
http://download.savannah.gnu.org/releases/tinycc/ 
and unzip the files into this src/tcc directory.

Use tc32.bat or tc64.bat to compile C files.

To build 32-bit MinForth enter:
  tc32 mf2c
  mf2c
  tc32 mf3
  mf3

To build 64-bit MinForth enter:
  tc64 mf2c
  mf2c
  tc64 mf3
  mf3
